from picamera import PiCamera
from openalpr import Alpr
import json, shlex, subprocess, cv2, sys, time

# Implementing the API of OpenALPR

RESOLUTION = (640, 480) # Camera resolution
IMG_DIR = 'alpr.jpg'

class PlateReader:

    def __init__(self):

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general parameters
        self.alpr_n = 5   #amount of plates to return

        # Initialise PiCam
        print("Initializing Camera...")
        start = time.time()
        self.camera = PiCamera()
        self.camera.resolution = RESOLUTION
        self.camera.start_preview()
        self.camera_started = True
        time.sleep(1.5)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")
    
    def capture_image(self):
        self.camera.capture(IMG_DIR)

    def init_alpr(self):
        print("Initializing OpenALPR...")
        start = time.time()
        # Instantiate OpenALPR
        self.alpr = Alpr(self.alpr_country, self.alpr_confdir, self.alpr_runtimedir)

        if not self.alpr.is_loaded():
            print("Error loading OpenALPR")
            sys.exit()

        self.alpr.set_top_n(self.alpr_n)
        self.alpr.set_default_region("md")
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

    def get_plate(self, img_dir):
        
        print("Analyzing image...")
        start = time.time()
        results = self.alpr.recognize_file(img_dir)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

        if results['results']:  #check if returned number plates is empty

            print("Results: ")
            i = 0
            for plate in results['results']:
                i += 1
                print("Plate #%d" % i)
                print("   %12s %12s" % ("Plate", "Confidence"))
                first_plate = str(plate['candidates'][0]['plate'])
                for candidate in plate['candidates']:
                    prefix = "-"
                    if candidate['matches_template']:
                        prefix = "*"

                    print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
        else:
            print ("No plates were detected.")
            
        print ("Done.")



if __name__=="__main__":
    plate_reader = PlateReader()
    plate_reader.init_alpr()
    total_time = 0
    i = 0

    while True:
        keyb_in = raw_input("\n1. Analyze image\nAny other key - Exit\n")
        if keyb_in == '1':
            start = time.time()
            plate_reader.capture_image()
            plate_reader.get_plate(IMG_DIR)
            end = time.time()
            total_time += end-start
            i += 1
            print("Capture and analysis took %f seconds" % (end-start))
        else:
            print("Average time per capture and analysis: %f" % (total_time/i))
            print("Exiting ... ")
            break
