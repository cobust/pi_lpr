import json, shlex, subprocess, cv2, time

# Changed over from webcam to RPi camera

class PlateReader:


    def __init__(self):
        # RPi cam rpistill args
        picam_command = "raspistill -w 640 -h 480 -t 1500 -n -o alpr.jpg"
        self.picam_command_args = shlex.split(picam_command)

        #alpr subprocess args
        alpr_command = "alpr -c eu -n 3 -j alpr.jpg" # removed "-t hr", compatibility issues
        self.alpr_command_args = shlex.split(alpr_command)

    def webcam_opencv(self):
        
        RESOLUTION = (640, 480)

        webcam = cv2.VideoCapture(0)
        # Make sure the webcam is device 0, otherwise change '0' in
        # VideoCapture(0) to the correct device number

        webcam.set(cv2.CAP_PROP_FRAME_WIDTH, RESOLUTION[0])              
        webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, RESOLUTION[1])
        time.sleep(0.67) # Waiting about 10 frames (@ 15fps capture)

        if webcam.isOpened() == False:               
            print ("Error accessing the webcam")     
            os.system("pause")

        is_read, frame = webcam.read()
        """
        # For benchmarking purposes
        is_read, frame = webcam.read()
        is_read, frame = webcam.read()
        is_read, frame = webcam.read()
        time.sleep(1)
        is_read, frame = webcam.read()
        time.sleep(3)
        is_read, frame = webcam.read()
        is_read, frame = webcam.read()
        is_read, frame = webcam.read()
        is_read, frame = webcam.read()
        time.sleep(5)
        is_read, frame = webcam.read()
        """
        

        if not is_read or frame is None:     
            print ("Unable to read frame from webcam")           
            os.system("pause")

        cv2.imwrite("alpr.jpg", frame)
        

    def alpr_subprocess(self):
        return subprocess.Popen(self.alpr_command_args, stdout=subprocess.PIPE)

    def picam_subprocess(self):
        return subprocess.Popen(self.picam_command_args, stdout=subprocess.PIPE)

    def alpr_json_results(self):
        #self.webcam_opencv()
        self.picam_subprocess().communicate() 
        alpr_out, alpr_error = self.alpr_subprocess().communicate()

        if not alpr_error is None:
            return None, alpr_error
        elif "No license plates found." in alpr_out:
            return None, None

        try:
            return json.loads(alpr_out), None
        except ValueError, e:
            return None, e

    
    def read_plate(self):
        alpr_json, alpr_error = self.alpr_json_results()

        if not alpr_error is None:
            print alpr_error
            return

        if alpr_json is None:
            print "No results!"
            return

        results = alpr_json["results"]

        ordinal = 0
        for result in results:
            candidates = result["candidates"]

            for candidate in candidates:
                #if candidate["matches_template"] == 1: # Not needed in this case
                ordinal += 1
                print "Guess {0:d}: {1:s} {2:.2f}%".format(ordinal, candidate["plate"], candidate["confidence"])


if __name__=="__main__":
    plate_reader = PlateReader()
    plate_reader.read_plate()
