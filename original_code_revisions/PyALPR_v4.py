from picamera.array import PiRGBArray
from picamera import PiCamera
import json, shlex, subprocess, cv2, time

# Loop implementation

RESOLUTION = (640, 480) # Camera resolution

class PlateReader:


    def __init__(self):
        
        print("Initialising ... ")
        start = time.time()
        #alpr subprocess args
        alpr_command = "alpr -c eu -n 3 -j alpr.jpg" # removed "-t hr", compatibility issues
        self.alpr_command_args = shlex.split(alpr_command)

        # Initialise PiCam
        self.camera = PiCamera()
        self.camera.resolution = RESOLUTION
        self.camera.start_preview()
        self.camera_started = True
        time.sleep(1.5)
        end = time.time()
        print("Done. Initialisation took %f seconds" %(end-start))
        print("System ready.")

    
    def capture_image(self):
        self.camera.capture('alpr.jpg')

    def alpr_subprocess(self):
        return subprocess.Popen(self.alpr_command_args, stdout=subprocess.PIPE)

    #@profile
    def alpr_json_results(self):
        self.capture_image()
        alpr_out, alpr_error = self.alpr_subprocess().communicate()

        if not alpr_error is None:
            return None, alpr_error
        elif "No license plates found." in alpr_out:
            return None, None

        try:
            return json.loads(alpr_out), None
        except ValueError, e:
            return None, e

    
    def read_plate(self):
        alpr_json, alpr_error = self.alpr_json_results()

        if not alpr_error is None:
            print alpr_error
            return

        if alpr_json is None:
            print "No results!"
            return

        results = alpr_json["results"]

        ordinal = 0
        for result in results:
            candidates = result["candidates"]

            for candidate in candidates:
                #if candidate["matches_template"] == 1: # Not needed in this case
                ordinal += 1
                print "Guess {0:d}: {1:s} {2:.2f}%".format(ordinal, candidate["plate"], candidate["confidence"])


if __name__=="__main__":
    plate_reader = PlateReader()

    while True:
        keyb_in = raw_input("\n1. Analyze image\nAny other key - Exit\n")

        if keyb_in == '1':
            start = time.time()
            plate_reader.read_plate()
            end = time.time()
            print("Done. Took %f seconds" % (end-start))
        else:
            print("Exiting ... ")
            break
