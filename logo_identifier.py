import cv2, sys, time

class LogoIdentifier:

    """
        Using a Cascade classifier identify vehicle logo
    """
    
    def __init__(self):
        
        self.vw_classifier = cv2.CascadeClassifier()
        self.vw_classifier.load("/home/pi/pi_lpr/cascade_xml/logos/vw_cascade.xml")
        if self.vw_classifier.empty():
            print("Error loading VW xml file. Exiting...")
            sys.exit()
        
        
    def detect(self, image, scale=1.2, strict=14):

        #start = time.time()
        results = self.vw_classifier.detectMultiScale(image, scale, strict)
        #end = time.time()
        #print(end-start)
        if len(results) != 0:
            return "Volkswagen"
        else:
            return "No logo detected."

if __name__ == "__main__":
    cc = LogoIdentifier()
    img = cv2.imread("alpr.jpg")
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cc.detect(grey)
