# Adapted from pyimagesearch.com

import time

class FPS:

    """
        Benchmarking tool to measure frame throughput of webcams/PiCams.

        Can count the amount of frames sampled within a certain time, and
        then calculates the theoretical FPS.
    """

    def __init__(self, time_limit=None):
        self.time_limit = time_limit
        self.start_time = None
        self.elapsed_time = None
        self.total_elapsed_time = None
        self.stop_time = None
        self.frames = 0

    def start(self):
        self.start_time = time.time()
        return self

    def stop(self):
        self.stop_time = time.time()
        self.total_elapsed_time = self.stop_time - self.start_time

    def count_frame(self):
        self.frames += 1

    def fps(self):
        return self.frames/self.total_elapsed_time

    def reset(self):
        self.start_time = None
        self.elapsed_time = None
        self.total_elapsed_time = None
        self.stop_time = None
        self.frames = 0

    def display_result(self, test_name="Test"):
        print("\n --- Results for " + test_name + " --- ")
        print("Frames sampled: " + str(self.frames))
        print("Elapsed time: " + str(self.total_elapsed_time) + " seconds.")
        print("Frame samples per second: " + str(self.fps()))
        
        
    
