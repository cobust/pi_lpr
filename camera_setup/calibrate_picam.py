from picamera.array import PiRGBArray
from picamera import PiCamera
import time, cv2

RESOLUTION = (640, 480)

# Rectangle draw parameters
x = RESOLUTION[0]/4
y = RESOLUTION[1]/4
w = RESOLUTION[0]/2
h = RESOLUTION[1]/2

# Initialise camera object
camera = PiCamera()
camera.resolution = RESOLUTION
camera.framerate = 32
raw = PiRGBArray(camera, size=RESOLUTION)
stream = camera.capture_continuous(raw, format="bgr", use_video_port=True)

time.sleep(0.2)

for frame in stream:

    image = frame.array
    
    cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),1)
    cv2.imshow("Calibrate", image)
    c = cv2.waitKey(1) % 0x100
    raw.truncate(0)

    if c == 27:
        stream.close()
        raw.close()
        camera.close()
        cv2.destroyAllWindows()
        break
        
        
