import cv2, os

"""
Simple code to help position the webcam correctly.
The goal is to save time and provide the ideal conditions for the LPR
"""

RESOLUTION = (640, 480)
x = RESOLUTION[0]/4
y = RESOLUTION[1]/4
w = RESOLUTION[0]/2
h = RESOLUTION[1]/2

webcam = cv2.VideoCapture(0)
# Make sure the webcam is device 0, otherwise change '0' in
# VideoCapture(0) to the correct device number

webcam.set(cv2.CAP_PROP_FRAME_WIDTH, RESOLUTION[0])              
webcam.set(cv2.CAP_PROP_FRAME_HEIGHT, RESOLUTION[1])

if webcam.isOpened() == False:               
    print ("Error accessing the webcam")     
    os.system("pause")                                                                                        

while cv2.waitKey(1) % 0x100 != 27 and webcam.isOpened():            

    is_read, frame = webcam.read()

    if not is_read or frame is None:     
        print ("Unable to read frame from webcam")           
        os.system("pause")                                      
    
    cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),1)
    cv2.imshow("calibrate", frame)
    
cv2.destroyAllWindows()
