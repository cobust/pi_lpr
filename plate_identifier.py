import numpy as np
import cv2, glob, sys

def draw_rect(roi, image, col=(0,255,0)):
    #Draw a rectangle(s) indicating ROI on the image
    #ROI(s) in this case is the output of the Cascade classifier in the PlateIdentifier class
    if len(roi) == 0:
        print('Cannot draw rectangle. Empty ROI given.')
        return None
            
    for (x,y,w,h) in roi:
            cv2.rectangle(image,(x,y),(x+w,y+h),col,2)
            
    return image


class PlateIdentifier:

    """
        Using a Cascade classifier to detect if an image contains a licence plate
    """
    
    def __init__(self, xml="/home/pi/openalpr/runtime_data/region/eu.xml"):
        self.classifier = cv2.CascadeClassifier()
        self.classifier.load(xml)
        if self.classifier.empty():
            print("Error loading xml file. Exiting...")
            sys.exit()
        self.positive_image = None
        self.roi = None
        
    def scan_image(self, image, scale=1.32, strict=3):

        # Return None if no plates were detected
        # Returns ROI if plate is detected
        
        #gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        results = self.classifier.detectMultiScale(image, scale, strict)
        if len(results) != 0:
            return True, results
        else:
            print("No plates detected!")
            return False, None

if __name__ == '__main__':
    cc = PlateIdentifier()
    data_dir = "/home/pi/raw_data/croatian_cars/*"
    file_list = glob.glob(data_dir)
    n_files = len(file_list)

    try:
        for path in file_list:

            cap_img = cv2.imread(path)
            is_plate, roi = cc.scan_image(cap_img)

            if is_plate:
                while (cv2.waitKey(1) % 0x100) != 27:
                    cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
                    cv2.imshow("Preview", draw_rect(image=cap_img, roi=roi))

                cv2.destroyWindow("Preview")
                cv2.waitKey(1)
    except KeyboardInterrupt:
        print("Ended.")
    
            
        
