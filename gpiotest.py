import RPi.GPIO as GPIO
import time

#@profile
def LEDtest():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(11, GPIO.OUT)

    try: 
        for i in range(1000):
            GPIO.output(11, GPIO.HIGH)
            GPIO.output(11, GPIO.LOW)
    except KeyboardInterrupt:
        GPIO.cleanup()
    GPIO.cleanup()

def LED_btn():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(11, GPIO.OUT) # LED at pin 11 (GPIO 17)
    GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Button at pin 13 (GPIO 27)

    try: 
        while True:
            if (GPIO.input(13) == 1):
                GPIO.output(11, GPIO.HIGH)
            else:
                GPIO.output(11, GPIO.LOW)
    except KeyboardInterrupt:
        GPIO.cleanup()

#LEDtest()
LED_btn()
