# Parts of the mapping code has been adapted from code suggested by
# Reddit user neihuffda (https://www.reddit.com/user/neihuffda)

from mpl_toolkits.basemap import Basemap
from prettytable import PrettyTable
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import matplotlib.cm as cm
import numpy as np
import time, urllib.request, urllib.error, subprocess, os, cv2, pymysql, sys

BASE_DIR = "/home/kojak"
MAPERITIVE_DIR = "/home/kojak/Downloads/Maperitive/Maperitive.exe"
work_dir = BASE_DIR


def sql():

    conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='pi_lpr')
    cur = conn.cursor()

    cur.execute("SELECT session_ID FROM sessions")


    print(cur.description)
    print("")
    for row in cur:
        print(row)

    cur.close()
    conn.close()
        

    #INSERT INTO `sessions` (`session_ID`, `user`, `no_of_scans`, `time_start`, `time_end`) VALUES ('', 'test_user', '2', CURRENT_TIMESTAMP, '2016-10-20 08:16:16')
    #INSERT INTO `scans` (`scan_ID`, `session_ID`, `licence_plate`, `make`, `col_hex`, `col_name`, `time_captured`, `lat`, `lon`) VALUES ('0', '1', 'WOBGO507', 'Volkswagen', '#ffee98', 'testcol', CURRENT_TIMESTAMP, '-33.833079', '18.796435'), ('1', '1', 'HGSLO631', 'Toyota', '#696969', 'slategray', CURRENT_TIMESTAMP, '-33.833076', '18.796435')

def upload_data():
    # upload data to database
    # Per single session only

    # connect to db
    conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='pi_lpr')
    cur = conn.cursor()

    # get sessions details
    sessions = []
    cur.execute("SELECT session_ID FROM sessions")
    for row in cur:
        sessions.append(int(row[0]))
    latest_session = max(sessions)+1

    # get scans details
    scans = []
    cur.execute("SELECT scan_ID FROM scans")
    for row in cur:
        scans.append(int(row[0]))
    latest_scan = max(scans)+1

    # get data from csv file
    data = []

    with open("gpsdata_realtime.txt", 'r') as f:
        
        scan_ID = latest_scan
        session_ID = latest_session
        licence_plate = "WOBGO507"
        make = "Volkswagen"
        col_hex = "#003366"
        col_name = "midnightblue"
        
        for line in f:

            split = line.split(',')
            lat = float(split[0])
            lon = float(split[1])
            data.append((scan_ID,session_ID,licence_plate,make,col_hex,col_name,lat,lon))
            scan_ID += 1

    # update session table
    exec_str = "INSERT INTO sessions VALUES (%d, 'test_user', %d, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)" % (latest_session, len(data))
    print(exec_str)
    cur.execute(exec_str)

    # update scans table
    for item in data:
        
        exec_str = "INSERT INTO scans VALUES (%d, %d, '%s', '%s', '%s', '%s', CURRENT_TIMESTAMP, %f, %f)" %(item[0],item[1],item[2],item[3],item[4],item[5],item[6],item[7])
        print(exec_str)
        cur.execute(exec_str)

    conn.commit()

    cur.close()
    conn.close()
    
def get_data(session=None):
    # connect to db
    conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='pi_lpr')
    cur = conn.cursor()

    exec_str = ""

    if session is None:
        # get all sessions
         exec_str = ("SELECT scans.scan_ID, scans.session_ID, sessions.user, scans.licence_plate, " 
                +"scans.make, scans.col_hex, scans.col_name, scans.time_captured, scans.lat, scans.lon FROM scans "
                +"INNER JOIN sessions ON scans.session_ID=sessions.session_ID")

    else:
        exec_str = ("SELECT scans.scan_ID, scans.session_ID, sessions.user, scans.licence_plate, " 
                +"scans.make, scans.col_hex, scans.col_name, scans.time_captured, scans.lat, scans.lon FROM scans "
                +"INNER JOIN sessions ON scans.session_ID=sessions.session_ID WHERE scans.session_ID=%d" %(int(session)))

    print(exec_str)
    cur.execute(exec_str)
    print("")

    # orginise data from SQL
    data = []
    mapping_data = []
    for row in cur:
        row_data = (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9])
        data.append(row_data)
        mapping_data.append((row[8],row[9],row[0]))

    # plot data in table for user
    t = PrettyTable(['Scan_ID', 'User', 'Licence plate', 'Make', 'Colour hex', 'Colour name','Time captured'])
    for item in data:
        t.add_row([item[0],item[2],item[3],item[4],item[5],item[6],item[7]])

    f = open('scan_data.txt', 'w')
    if session is None:
        f.write("Scan data for all sessions.\n")
    else:
        f.write("Scan data for Session %d.\n" %(int(session)))

    f.write(str(t))
    f.close()

    map_data(mapping_data)
    

def map_data(data):

    online = False
    try:
        response=urllib.request.urlopen('http://www.openstreetmap.org/', timeout=1)
        online = True
        print("Connection to OSM achieved")
    except urllib.error.URLError:
        print("No internet connection")

    # values to plot
    x = [] # lat values of markers
    y = [] # long values of markers
    labels = []

    for row in data:
        x.append(row[0])
        y.append(row[1])
        labels.append(row[2])

    # central longitude and latitude of the projection
    lat_0 = (sum(x)/float(len(x)))
    lon_0 = (sum(y)/float(len(y)))

    # NOTE: 
    # Some of the code below has been adapted from Reddit user
    # neihuffda's (https://www.reddit.com/user/neihuffda) mapping code

    # prep to get topright and borleft corners of map
    pad = 1*(max(y) - min(y))
    scale = int((max(y) + min(y))/2)

    # lat/lon values of the lower left and upper right corners of the map
    llcrnrlat = min(x) - pad
    llcrnrlon = min(y) - pad
    urcrnrlat = max(x) + pad
    urcrnrlon = max(y) + pad

    if online:
        print("Retreving OSM map...")
        wget_command = 'wget --show-progress -q -O kart.osm "http://overpass-api.de/api/map?bbox=%f,%f,%f,%f"' % (llcrnrlon,llcrnrlat,urcrnrlon,urcrnrlat)
                                                                                                                                                                                                                          
        #"http://api.openstreetmap.org/api/0.6/map?bbox=%s,%s,%s,%s"
        subprocess.call(wget_command, shell=True)

        bound_string="%s, %s, %s, %s" % (llcrnrlon,llcrnrlat,urcrnrlon,urcrnrlat)
        master_string="clear-map\nchange-directory %s/\nload-source %s/kart.osm\nuse-ruleset alias=default\nset-print-bounds-geo %s\nexport-bitmap scale=10 file=gpsdraw.png\n" % (work_dir, work_dir, bound_string)

        filename = 'tegngps.mscript'
        file = open(filename, 'w')
        file.write(master_string)
        file.close()
        
        print("Converting map...")
        fnull = open(os.devnull, 'w')
        maperative = "mono %s" % (MAPERITIVE_DIR)
        map_command = ' -exitafter -defaultscript %s/tegngps.mscript' % work_dir
        subprocess.call(maperative+map_command, stdout=fnull, stderr=subprocess.STDOUT, shell=True)

    if online:
        res = 'l'
    else:
        res = 'h'

    bmap = Basemap(projection='merc', lat_0=lat_0, lon_0=lon_0,resolution=res, area_thresh=0.1,
                   llcrnrlon=llcrnrlon, llcrnrlat=llcrnrlat,urcrnrlon=urcrnrlon, urcrnrlat=urcrnrlat)

    if online == False:
        bmap.drawcoastlines()
        bmap.drawcountries()
        bmap.drawmapboundary(fill_color='aqua')
        bmap.fillcontinents(color='green', lake_color='aqua')

    # convert coords and plot
    x_,y_ = bmap(y, x)
    bmap.plot(x_, y_, 'bo', label='awe', markersize=3) # plot locations

    # plot scan_ID's
    for i in range(len(labels)):
        plt.text(x_[i]-0.1,y_[i]+0.1, labels[i],fontsize=8,fontweight='normal',ha='center',va='center',color='r')
                    
    # add OSM map background       
    if response:
        img = cv2.imread("gpsdraw.png")
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        bmap.imshow(img, zorder=1, origin='upper')
    else:
        pass

    plt.title("Title here")
    plt.plot()
    plt.show()
    #plt.savefig('%s.png' % gps_file, bbox_inches='tight', dpi=500)

    # Delete files
    #subprocess.call('rm -rf kart.osm tegngps.mscript gpsdraw.png' , stdout=fnull, stderr=subprocess.STDOUT, shell=True)

    fnull.close()

get_data()
