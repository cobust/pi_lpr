import time, sys
from gps_lib import GPSFeed

OUTPUT_FILE = "gps_data.txt"
f_r = open("gpsdata_realtime.txt", 'w')
f_r.close()
f = open(OUTPUT_FILE, 'w')
f.close()
print("Waiting for 30 seconds ... ")
time.sleep(30)
gps = GPSFeed().start()

"""
f = open(OUTPUT_FILE, "w")
f.write("Lat, Long, Alt, Speed, Fix")
f.close()
"""
print("waiting for 600 seconds...")
time.sleep(300)

def realtime():
    while True:
        try:

            write_str = "%s,%s,%s,%s,%s\n" % (str(gps.lat), str(gps.long), str(gps.alt), str(gps.speed), str(gps.fix_mode))

            f = open(OUTPUT_FILE, 'a')
            f.write(write_str)
            f.close()
            print(write_str)
            time.sleep(1.1)
            
        except KeyboardInterrupt:
            print("Exiting...")
            gps.stop()

    print("Done.")

def estimate():
    
    count = 0
    lat_buf = []
    long_buf = []
    avg_lat = None
    avg_long = None
    
    while True:
        try:
            write_str = "%s,%s,%s,%s,%s\n" % (str(gps.lat), str(gps.long), str(gps.alt), str(gps.speed), str(gps.fix_mode))

            f_r = open("gpsdata_realtime.txt", 'a')
            f_r.write(write_str)
            f_r.close()
            print(gps.fix_mode)
            
            if gps.fix_mode == 3:
                lat_buf.append(gps.lat)
                long_buf.append(gps.long)
            else:
                #reset when fix is lost
                
                if len(lat_buf) > 0 and len(long_buf) > 0:
                    avg_lat = sum(lat_buf)/float(len(lat_buf))
                    avg_long = sum(long_buf)/float(len(long_buf))
                    write_str = "%s,%s,%s\n" % (str(avg_lat), str(avg_long), str(gps.fix_mode))
                    print("Fix lost. "+write_str)
                    f = open(OUTPUT_FILE, 'a')
                    f.write(write_str)
                    f.close()
                    
                lat_buf = []
                long_buf = []
                count = 0

            if count == 10 and gps.fix_mode == 3:

                count = 0
                avg_lat = sum(lat_buf)/float(len(lat_buf))
                avg_long = sum(long_buf)/float(len(long_buf))
                write_str = "%s,%s,%s\n" % (str(avg_lat), str(avg_long), str(gps.fix_mode))
                print(write_str)
                f = open(OUTPUT_FILE, 'a')
                f.write(write_str)
                f.close()
                lat_buf = []
                long_buf = []
                
            time.sleep(1.1)
            count += 1
            
        except KeyboardInterrupt:
            print("Exiting...")
            gps.stop()

    print("Done.")

estimate()
        

