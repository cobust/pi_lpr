import numpy as np
import glob, time, cv2, time, sys, utm


f2 = open("utm_data.txt","w")
with open("gps_data.txt", 'r') as f:
    for line in f:
        
        split = line.split(',')
        lat = float(split[0].strip())
        longi = float(split[1].strip())

        result = utm.from_latlon(lat, longi)
        
        write_str = "%f,%f\n" % (result[0],result[1])
        f2.write(write_str)

f2.close()
        

#print(utm.from_latlon(-33.834060, 18.799045))
