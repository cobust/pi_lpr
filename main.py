from gps_lib import GPSFeed
from openalpr import Alpr
from collections import deque
from FPS import FPS
from threading import Thread
from picam_thread import PiCamStream
from plate_identifier import *
from logo_identifier import *
from feature_extraction import *
import RPi.GPIO as GPIO
import Adafruit_CharLCD as LCD
import time, sys, cv2, glob, os

ALPR_BASE_DIR = "/home/pi/pi_lpr/alpr_output/"
# LED's
GREEN_LED = 13
YELLOW_LED = 16

# Buttons
LEFT_BTN = 21
MIDDLE_BTN = 20
RIGHT_BTN = 19

def start_alpr():
    os.system("python alpr_lib.py")

class LPR():

    def __init__(self):
        self.init_IO()
        #plate image directory
        self.img_dir = "alpr_img.jpg"
        self.img_dir_actual = "/home/pi/pi_lpr/alpr_img.jpg"
        self.saved_img = None

        #test plate reference
        self.control = "OS802HN"
        self.n_correct = 0
        self.n_tests = 0

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general parameters
        self.alpr_n = 5   #amount of plates to return

        #picam stream
        self.stream = None
        self.stream_active = False

        #Plate detector (cascade classifier)
        self.detector = None

    def init_IO(self):

        # Raspberry Pi pin configuration:
        lcd_rs        = 27  # Note this might need to be changed to 21 for older revision Pi's.
        lcd_en        = 22
        lcd_d4        = 25
        lcd_d5        = 24
        lcd_d6        = 23
        lcd_d7        = 18

        # Define LCD column and row size for 16x2 LCD.
        lcd_columns = 16
        lcd_rows    = 2

        # Alternatively specify a 20x4 LCD.
        # lcd_columns = 20
        # lcd_rows    = 4

        # Initialize the LCD using the pins above.
        self.lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,
                                   lcd_columns, lcd_rows)

        # LED's and button setup
        GPIO.setup(GREEN_LED, GPIO.OUT)
        GPIO.setup(YELLOW_LED, GPIO.OUT)
        
        GPIO.setup(LEFT_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(MIDDLE_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(RIGHT_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        self.lcd.message("INITIALISING...")
        
    def init_alpr(self):
        """
        print("Initializing OpenALPR...")
        start = time.time()
        self.alpr = Alpr(self.alpr_country, self.alpr_confdir, self.alpr_runtimedir)

        if not self.alpr.is_loaded():
            print("Error loading OpenALPR")
            sys.exit()

        self.alpr.set_top_n(self.alpr_n)
        self.alpr.set_default_region("md")
        end = time.time()
        #print("Done. Took " + str(end - start) + " seconds.\n")
        
        """
        Thread(target=start_alpr, args=()).start()

    def init_stream(self):
        print("Starting stream...")
        self.stream = PiCamStream().start()
        self.stream_active = True

    def stop_stream(self):
        self.stream.stop()
        self.stream_active = False

    def init_detector(self):
        print("Initializing detectors...")
        self.detector = PlateIdentifier()
        self.logo_detector = LogoIdentifier()

    def get_frame(self):
        return self.stream.get_frame()

    def save_image(self, image):
        print("Saving image...")
        cv2.imwrite(self.img_dir, image)

    def get_plate_new(self, img):
        
        results = []
        start_time = time.time()
        
        while True:
            
            file_list = glob.glob(ALPR_BASE_DIR+"*")

            if ALPR_BASE_DIR+"is_loaded" in file_list: # If ALPR is loaded
                #print("ALPR loaded")
                if ALPR_BASE_DIR+"done" in file_list: # If results is ready to read
                    
                    count = 0
                    with open(ALPR_BASE_DIR+"results.dat", 'r') as res:
                        for line in res:
                            results.append(line.rstrip())
                            
                    os.remove(ALPR_BASE_DIR+"done")
                    os.remove(ALPR_BASE_DIR+"analysing")
                    break

                else: # If resuts not ready to read
                    #print("Not done")
                    if ALPR_BASE_DIR+"img_ready" not in file_list: # If ALPR is not busy analysing
                        print("Writing img")
                        cv2.imwrite(ALPR_BASE_DIR+"alpr.jpg", img)
                        f = open(ALPR_BASE_DIR+"img_ready", 'w')
                        f.close()
                    else:
                        if (time.time()-start_time) > 8:
                            print("ALPR error! Done file not found in  seconds.")
                        time.sleep(0.1)
                
            else:
                if (time.time()-start_time) > 8:
                    print("ALPR error! Not loaded, in infinite loop")
                time.sleep(0.1)
            

        return results
    
    def get_plate(self, image=None):
        correct = False
        print("Analyzing image...")
        start = time.time()
        results = self.alpr.recognize_file(self.img_dir_actual)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

        if results['results']:  #check if returned number plates is empty
            print("Results: ")
            i = 0
            for plate in results['results']:
                i += 1
                print("Plate #%d" % i)
                print("   %12s %12s" % ("Plate", "Confidence"))
                first_plate = str(plate['candidates'][0]['plate'])
                if first_plate == self.control:
                    correct = True
                    print("True!")
                for candidate in plate['candidates']:
                    prefix = "-"
                    if candidate['matches_template']:
                        prefix = "*"

                    print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
        else:
            print ("No plates were detected.")
            
        print ("Done.")
        return correct
#@profile
def benchmark():
    read_plate = LPR()
    read_plate.init_stream()
    read_plate.init_detector()
    read_plate.init_alpr()
    gpsd = GPSFeed().start()


    for testterr in range(101):
        
        if True:
                
            ready = False
            captured_image = None
            captured_roi = None
            captured = False

            false_streak = 0
            len_fb = 0
            len_frb = 0
        
            max_buffer_length = 10 # Max amount of frames/results stored in buffers specified below. To limit memory usage
            ready_threshold = 6
            false_streak_threshold = 4
            frame_buffer = deque() # Past frames are stored in this list
            frame_result_buffer = deque() # Corresponds with frame_buffer, results of PlateIdentifier stored here
            frame_roi_buffer = deque()
                       
            # Start camera stream if not started yet
            if not read_plate.stream_active:
                read_plate.init_stream()

            fps = FPS().start()
            
            try:
                while not captured:

                    # If system is ready to take a successful picture, LED on
                    if ready:
                        GPIO.output(11, GPIO.HIGH)
                    else:
                        GPIO.output(11, GPIO.LOW)

                    len_fb = len(frame_buffer)
                    len_frb = len(frame_result_buffer)
                    
                    if len_fb >= max_buffer_length:
                        if len_fb > 0 and len_frb > 0: # safety check to not pop from empty queue
                            #print(len_fb-1, len_frb-1, type(len_fb-1), type(len_frb-1))
                            # remove oldest frame and result in buffer
                            frame_buffer.popleft()
                            frame_result_buffer.popleft()
                            frame_roi_buffer.popleft()

                    frame = read_plate.get_frame() # get a new frame
                    is_plate, roi = read_plate.detector.scan_image(frame) # check if a licence plate is present in image
                    
                    # if no plate detected
                    if is_plate:
                        false_streak = 0
                    else:
                        false_streak += 1
                        
                    # Add latest frame and result to buffers
                    frame_buffer.append(frame)
                    frame_result_buffer.append(is_plate)
                    frame_roi_buffer.append(roi)

                    """
                    # Show output with ROI
                    if is_plate:
                        show_img = frame
                        cv2.imshow('stream',draw_rect(roi, show_img))
                        cv2.waitKey(1)
                    else:
                        cv2.imshow('stream',frame)
                        cv2.waitKey(1)
                    """
                    

                    # If desired threshold is reached
                    if frame_result_buffer.count(True) >= ready_threshold:
                        ready = True

                    # If a False streak is detected, system not reafy
                    if false_streak >= false_streak_threshold:
                        ready = False

                    fps.count_frame()

                    # START<USER INTERACTION
                    # If external button is pressed
                    if True:
                        # If system is ready
                        if ready:
                            fps.stop()
                            # Get image with positive plate result
                            for i in range(len_fb):

                                # If latest image has a plate
                                #print (frame_result_buffer[i])
                                if frame_result_buffer[i]:
                                    read_plate.save_image(frame_buffer[i])
                                    captured_image = frame_buffer[i]
                                    captured_roi = frame_roi_buffer[i]
                                    captured = True
                                    break
                        else:
                            print("System not ready!")
                    # USER INTERACTION>END


            except KeyboardInterrupt:
                pass

            # Get licence plate
            start=time.time()
            read_plate.get_plate()
            #results = read_plate.get_plate_new(captured_image)
            end=time.time()
            #print("Time to get plate: %f " %(end-start))
            #print("Possible plates:")
            #for item in results:
                #print("  - "+item)

            # Image pre-processing
            img = illum_norm(captured_image)
            grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Identify logo
            start=time.time()
            logo = read_plate.logo_detector.detect(grey)
            end=time.time()

            # Identify plate area, TODO: should not be here
            #is_plate, roi = read_plate.detector.scan_image(grey)

            if is_plate:
                avg_top, closest_col, alternate_cols = colour_extraction(captured_roi, img)
                print("")
                print("Detected color(region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
                print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
                print("Alternate colour 1: "+str(alternate_cols[0]))
                print("Alternate colour 2: "+str(alternate_cols[1]))

            print("")
            print("Vehicle make: " + logo)
            print("Time to detect logo: %f" % (end-start))

            print("")
            print ("GPS Mode: ", gpsd.fix_mode)
            print("Plate was captured at: ")

            if gpsd.fix_mode != 1:
                print ('Location: ', gpsd.est_loc)
            else:
                print("NO GPS FIX!")
                
            #fps.display_result(test_name="Main processing loop")
            #osm = OSMHandler()
            #osm.prep_node(lat, long, "plate here")

            # Reset GPIO
            GPIO.cleanup()
    print("Main program closing...")
    read_plate.stop_stream()
    gpsd.stop()
    #read_plate.alpr.unload()  #release memory
    f = open(ALPR_BASE_DIR+"exit", 'w')
    f.close()
        
if __name__=="__main__":
    
    start=time.time()
    read_plate = LPR()
    read_plate.init_stream()
    read_plate.init_detector()
    read_plate.init_alpr()
    gpsd = GPSFeed().start()
    end=time.time()
    print("Done. Took " + str(end - start) + " seconds.\n")

    print("System ready.")
    read_plate.lcd.clear()
    read_plate.lcd.message("Ready.")

    while True:
        keyb_in = raw_input("\n1. Analyze single image\n2. View saved image\n3. Automated capture and process"
                            +"\n4. Final system\nAny other key - Exit\n")
        
        if keyb_in == '1':
            frame = read_plate.get_frame()
            read_plate.save_image(frame)
            rand_var = read_plate.get_plate()
            
        elif keyb_in == '2':
            print("\nESC to exit preview.")
            cap_img = cv2.imread(read_plate.img_dir_actual)
            is_plate, roi = read_plate.detector.scan_image(cap_img)

            if is_plate:
                while (cv2.waitKey(1) % 0x100) != 27:
                    cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
                    cv2.imshow("Preview", draw_rect(image=cap_img, roi=roi))

                cv2.destroyWindow("Preview")
                cv2.waitKey(1)
                
        elif keyb_in == '3':
            n_consecutive_plates = 0
            frame = None
            ready = False
            captured = False
            
            while captured is False:

                try:
                    frame = read_plate.get_frame()
                    is_plate, roi = read_plate.detector.scan_image(frame)
                    
                    if is_plate:
                        n_consecutive_plates += 1
                    else:
                        n_consecutive_plates = 0
                        ready = False
                        
                    if n_consecutive_plates >= 5:
                        read_plate.save_image(frame)
                        ready = True

                    if ready:
                        print("READY")
                except KeyboardInterrupt:
                    pass
        
            while (cv2.waitKey(1) % 0x100) != 27:
                    cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
                    cv2.imshow("Preview", draw_rect(image=frame, roi=roi))

            cv2.destroyWindow("Preview")
            cv2.waitKey(1)
            read_plate.get_plate()

        elif keyb_in == '4':

            ready = False
            captured_image = None
            captured_roi = None
            captured = False

            false_streak = 0
            len_fb = 0
            len_frb = 0
        
            max_buffer_length = 10 # Max amount of frames/results stored in buffers specified below. To limit memory usage
            ready_threshold = 6
            false_streak_threshold = 4
            frame_buffer = deque() # Past frames are stored in this list
            frame_result_buffer = deque() # Corresponds with frame_buffer, results of PlateIdentifier stored here
            frame_roi_buffer = deque() # ROI's of frames are stored here
                        
            # Start camera stream if not started yet
            if not read_plate.stream_active:
                read_plate.init_stream()

            fps = FPS().start()
            
            try:
                while not captured:

                    # If system is ready to take a successful picture, LED on
                    if ready:
                        GPIO.output(GREEN_LED, GPIO.HIGH)
                    else:
                        GPIO.output(GREEN_LED, GPIO.LOW)

                    len_fb = len(frame_buffer)
                    len_frb = len(frame_result_buffer)
                    
                    if len_fb >= max_buffer_length:
                        if len_fb > 0 and len_frb > 0: # safety check to not pop from empty queue
                            #print(len_fb-1, len_frb-1, type(len_fb-1), type(len_frb-1))
                            # remove oldest frame and result in buffer
                            frame_buffer.popleft()
                            frame_result_buffer.popleft()
                            frame_roi_buffer.popleft()

                    frame = read_plate.get_frame() # get a new frame
                    is_plate, roi = read_plate.detector.scan_image(frame) # check if a licence plate is present in image
                    
                    # if no plate detected
                    if is_plate:
                        false_streak = 0
                    else:
                        false_streak += 1
                        
                    # Add latest frame and result to buffers
                    frame_buffer.append(frame)
                    frame_result_buffer.append(is_plate)
                    frame_roi_buffer.append(roi)


                    # Show output with ROI
                    if is_plate:
                        show_img = frame
                        cv2.imshow('stream',draw_rect(roi, show_img))
                        cv2.waitKey(1)
                    else:
                        cv2.imshow('stream',frame)
                        cv2.waitKey(1)

                    

                    # If desired threshold is reached
                    if frame_result_buffer.count(True) >= ready_threshold:
                        ready = True

                    # If a False streak is detected, system not reafy
                    if false_streak >= false_streak_threshold:
                        ready = False

                    fps.count_frame()

                    # START<USER INTERACTION
                    # If external button is pressed
                    if (GPIO.input(MIDDLE_BTN)==1):
                        # If system is ready
                        if ready:
                            fps.stop()
                            # Get image with positive plate result
                            for i in range(len_fb):

                                # If latest image has a plate
                                #print (frame_result_buffer[i])
                                if frame_result_buffer[i]:
                                    read_plate.lcd.clear()
                                    read_plate.save_image(frame_buffer[i])
                                    captured_image = frame_buffer[i]
                                    captured_roi = frame_roi_buffer[i]
                                    captured = True
                                    GPIO.output(GREEN_LED, GPIO.LOW)
                                    read_plate.lcd.message("ANALYSING...")
                                    break
                        else:
                            print("System not ready!")
                    # USER INTERACTION>END


            except KeyboardInterrupt:
                pass

            # Get licence plate
            start=time.time()
            #read_plate.get_plate()
            results = read_plate.get_plate_new(captured_image)
            read_plate.lcd.clear()
            end=time.time()
            print("Time to get plate: %f " %(end-start))
            print("Possible plates:")
            read_plate.lcd.message(str(results[0]))
            for item in results:
                print("  - "+item)

            # Image pre-processing
            img = illum_norm(captured_image)
            grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Identify logo
            start=time.time()
            logo = read_plate.logo_detector.detect(grey)
            end=time.time()

            # Identify plate area, TODO: should not be here
            #is_plate, roi = read_plate.detector.scan_image(grey)

            if is_plate:
                avg_top, closest_col, alternate_cols = colour_extraction(captured_roi, img)
                print("")
                print("Detected color(region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
                print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
                print("Alternate colour 1: "+str(alternate_cols[0]))
                print("Alternate colour 2: "+str(alternate_cols[1]))

            print("")
            print("Vehicle make: " + logo)
            print("Time to detect logo: %f" % (end-start))

            print("")
            print ("GPS Mode: ", gpsd.fix_mode)
            print("Plate was captured at: ")

            if gpsd.fix_mode != 1:
                print ('Location: ', gpsd.est_loc)
            else:
                print("NO GPS FIX!")
                
            fps.display_result(test_name="Main processing loop")
            #osm = OSMHandler()
            #osm.prep_node(lat, long, "plate here")
            
        else:
            print("Main program closing...")
            read_plate.stop_stream()
            gpsd.stop()
            #read_plate.alpr.unload()  #release memory
            f = open(ALPR_BASE_DIR+"exit", 'w')
            f.close()
            GPIO.cleanup()
            sys.exit()

    #benchmark()
