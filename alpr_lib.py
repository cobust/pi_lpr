from openalpr import Alpr
from threading import Thread
import time, sys, glob, os

BASE_DIR = "/home/pi/pi_lpr/alpr_output/"

class ALPR:

    def __init__(self):

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general parameters
        self.alpr_n = 5   #amount of plates to return

        print("Initializing OpenALPR...")
        start = time.time()
        # Instantiate OpenALPR
        self.alpr = Alpr(self.alpr_country, self.alpr_confdir, self.alpr_runtimedir)

        if not self.alpr.is_loaded():
            print("Error loading OpenALPR")
            sys.exit()

        self.alpr.set_top_n(self.alpr_n)
        
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

    def set_region(self, region):
        self.alpr.set_default_region(region)

    def write_results(self, results):

        f = open(BASE_DIR+"results.dat", 'w')
        if results['results']:  #check if returned number plates is empty
            all_plates = results['results'][0]

            # Choose the plate that has the highest probability and matches the region
            for candidate in all_plates['candidates']:
                f.write(str(candidate['plate'])+"\n")
        else:
            f.write("None")
            
        f.close()

        done = open(BASE_DIR+"done", "w")
        done.close()

    def get_best_plate(results):
        best_plate = None
        if results['results']:  #check if returned number plates is empty
            all_plates = results['results'][0]

            # Choose the plate that has the highest probability and matches the region
            for candidate in all_plates['candidates']:
                if candidate['matches_template']:
                    best_plate = str(candidate['plate'])
                    break

            # If no plates matches pattern, choose one with highest probability
            if best_plate is None:
                best_plate = str(all_plates['candidates'][0]['plate'])

        return best_plate

    def get_results(self, img_dir):
        results = self.alpr.recognize_file(img_dir)
        return results

    def get_plate(self, img_dir):
        self.write_results(self.get_results(img_dir))


def run():
    alpr = ALPR()
    f = open(BASE_DIR+"is_loaded", 'w')
    f.close()

    file_list = glob.glob(BASE_DIR+"*")

    if BASE_DIR+"exit" in file_list:
        os.remove(BASE_DIR+"exit")

    while True:
    
        file_list = glob.glob(BASE_DIR+"*")

        if BASE_DIR+"exit" in file_list:
            os.remove(BASE_DIR+"exit")
            break

        if BASE_DIR+"img_ready" in file_list:
            print("Ready!")
            f = open(BASE_DIR+"analysing", 'w')
            f.close()
            alpr.get_plate(BASE_DIR+"alpr.jpg")
            os.remove(BASE_DIR+"img_ready")


        """
        if BASE_DIR+"done" in file_list:
            with open(BASE_DIR+"results.dat", 'r') as f:
                for line in f:
                    print(line.rstrip())
                    
            os.remove(BASE_DIR+"done")
        """
            
        time.sleep(0.8)
        
    print("Exiting...")
    os.remove(BASE_DIR+"is_loaded")
    alpr.alpr.unload()

if __name__ == '__main__':
    run()

    
