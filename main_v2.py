from gps_lib import GPSFeed
from openalpr import Alpr
from collections import deque
from FPS import FPS
from threading import Thread
from picam_thread import PiCamStream
from plate_identifier import *
from logo_identifier import *
from feature_extraction import *
import RPi.GPIO as GPIO
import Adafruit_CharLCD as LCD
import time, sys, cv2, glob, os

ALPR_BASE_DIR = "/home/pi/pi_lpr/alpr_output/"
# LED's
GREEN_LED = 13
YELLOW_LED = 16

# Buttons
LEFT_BTN = 21
MIDDLE_BTN = 20
RIGHT_BTN = 19

def start_alpr():
    os.system("python alpr_lib.py")

class LPR():

    def __init__(self):
        self.init_IO()
        self.lcd_update("INIT...")
        
        #plate image directory
        self.img_dir = "alpr_img.jpg"
        self.img_dir_actual = "/home/pi/pi_lpr/alpr_img.jpg"
        self.saved_img = None

        #test plate reference
        self.control = "OS802HN"
        self.n_correct = 0
        self.n_tests = 0

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general parameters
        self.alpr_n = 5   #amount of plates to return

        #picam stream
        self.stream = None
        self.stream_active = False

        #Plate detector (cascade classifier)
        self.detector = None

    def init_IO(self):

        # Raspberry Pi pin configuration:
        lcd_rs        = 27  # Note this might need to be changed to 21 for older revision Pi's.
        lcd_en        = 22
        lcd_d4        = 25
        lcd_d5        = 24
        lcd_d6        = 23
        lcd_d7        = 18

        # Define LCD column and row size for 16x2 LCD.
        lcd_columns = 16
        lcd_rows    = 2

        # Alternatively specify a 20x4 LCD.
        # lcd_columns = 20
        # lcd_rows    = 4

        # Initialize the LCD using the pins above.
        self.lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,
                                   lcd_columns, lcd_rows)
        self.lcd_msg = ""
        self.lcd.message(self.lcd_msg)

        # LED's and button setup
        GPIO.setup(GREEN_LED, GPIO.OUT)
        GPIO.setup(YELLOW_LED, GPIO.OUT)
        
        GPIO.setup(LEFT_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(MIDDLE_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(RIGHT_BTN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        
    def init_alpr(self):
        Thread(target=start_alpr, args=()).start()

    def init_stream(self):
        print("Starting stream...")
        self.stream = PiCamStream().start()
        self.stream_active = True

    def stop_stream(self):
        self.stream.stop()
        self.stream_active = False

    def init_detector(self):
        print("Initializing detectors...")
        self.detector = PlateIdentifier()
        self.logo_detector = LogoIdentifier()

    def get_frame(self):
        return self.stream.get_frame()

    def save_image(self, image):
        print("Saving image...")
        cv2.imwrite(self.img_dir, image)

    def lcd_update(self, msg):
        if msg == self.lcd_msg:
            pass
        else:
            
            if self.lcd_msg != "":
                self.lcd.clear()
            time.sleep(0.07)
            if len(msg) > 16:
                self.lcd.message(msg[:7]+"\n"+msg[8:15])
            elif len(msg) > 8:
                self.lcd.message(msg[:7]+"\n"+msg[8:])
            elif len(msg) <= 8:
                 self.lcd.message(msg)
                 
            self.lcd_msg = msg

    def get_plate(self, img):
        
        results = []
        start_time = time.time()
        
        while True:
            
            file_list = glob.glob(ALPR_BASE_DIR+"*")

            if ALPR_BASE_DIR+"is_loaded" in file_list: # If ALPR is loaded
                #print("ALPR loaded")
                if ALPR_BASE_DIR+"done" in file_list: # If results is ready to read
                    
                    count = 0
                    with open(ALPR_BASE_DIR+"results.dat", 'r') as res:
                        for line in res:
                            results.append(line.rstrip())
                            
                    os.remove(ALPR_BASE_DIR+"done")
                    os.remove(ALPR_BASE_DIR+"analysing")
                    break

                else: # If resuts not ready to read
                    #print("Not done")
                    if ALPR_BASE_DIR+"img_ready" not in file_list: # If ALPR is not busy analysing
                        print("Writing img")
                        cv2.imwrite(ALPR_BASE_DIR+"alpr.jpg", img)
                        f = open(ALPR_BASE_DIR+"img_ready", 'w')
                        f.close()
                    else:
                        if (time.time()-start_time) > 8:
                            print("ALPR error! Done file not found in  seconds.")
                        time.sleep(0.1)
                
            else:
                if (time.time()-start_time) > 8:
                    print("ALPR error! Not loaded, in infinite loop")
                time.sleep(0.1)
                
        return results
    
        
if __name__=="__main__":
    
    file_list = glob.glob(ALPR_BASE_DIR+"*")
    if ALPR_BASE_DIR+"is_loaded" in file_list: # If ALPR is falsely loaded
        os.remove(ALPR_BASE_DIR+"is_loaded")
        
    start=time.time()
    read_plate = LPR()
    read_plate.init_stream()
    read_plate.init_detector()
    read_plate.init_alpr()
    gpsd = GPSFeed().start()
    end=time.time()
    print("Done. Took " + str(end - start) + " seconds.\n")

    print("Main program ready.")

    # Wait for ALPR to load
    while True:

        file_list = glob.glob(ALPR_BASE_DIR+"*")
        if ALPR_BASE_DIR+"is_loaded" in file_list: # If ALPR is loaded
            print("ALPR ready.")
            break
        else:
            time.sleep(0.2)

    try:
        # Main program loop
        while True:
            read_plate.lcd_update("Waiting...")
            
            ready = False
            captured_image = None
            captured_roi = None
            captured = False

            false_streak = 0
            len_fb = 0
            len_frb = 0
        
            max_buffer_length = 10 # Max amount of frames/results stored in buffers specified below. To limit memory usage
            ready_threshold = 6
            false_streak_threshold = 4
            frame_buffer = deque() # Past frames are stored in this list
            frame_result_buffer = deque() # Corresponds with frame_buffer, results of PlateIdentifier stored here
            frame_roi_buffer = deque() # ROI's of frames are stored here
                        
            # Start camera stream if not started yet
            if not read_plate.stream_active:
                read_plate.init_stream()

            fps = FPS().start()
            
            # Stream analysis loop
            while not captured:

                # If system is ready to take a successful picture, LED on
                if ready:
                    GPIO.output(GREEN_LED, GPIO.HIGH)
                else:
                    GPIO.output(GREEN_LED, GPIO.LOW)

                len_fb = len(frame_buffer)
                len_frb = len(frame_result_buffer)
                
                if len_fb >= max_buffer_length:
                    if len_fb > 0 and len_frb > 0: # safety check to not pop from empty queue
                        #print(len_fb-1, len_frb-1, type(len_fb-1), type(len_frb-1))
                        # remove oldest frame and result in buffer
                        frame_buffer.popleft()
                        frame_result_buffer.popleft()
                        frame_roi_buffer.popleft()

                frame = read_plate.get_frame() # get a new frame
                is_plate, roi = read_plate.detector.scan_image(frame) # check if a licence plate is present in image
                
                # if no plate detected
                if is_plate:
                    false_streak = 0
                else:
                    false_streak += 1
                    
                # Add latest frame and result to buffers
                frame_buffer.append(frame)
                frame_result_buffer.append(is_plate)
                frame_roi_buffer.append(roi)

                # Show output with ROI
                if is_plate:
                    show_img = frame
                    cv2.imshow('stream',draw_rect(roi, show_img))
                    cv2.waitKey(1)
                else:
                    cv2.imshow('stream',frame)
                    cv2.waitKey(1)

                # If desired threshold is reached
                if frame_result_buffer.count(True) >= ready_threshold:
                    ready = True

                # If a False streak is detected, system not reafy
                if false_streak >= false_streak_threshold:
                    ready = False

                fps.count_frame()

                # START<USER INTERACTION
                # If external button is pressed
                if (GPIO.input(MIDDLE_BTN)==1):
                    # If system is ready
                    if ready:
                        fps.stop()
                        # Get image with positive plate result
                        for i in range(len_fb):

                            # If latest image has a plate
                            #print (frame_result_buffer[i])
                            if frame_result_buffer[i]:
                                read_plate.save_image(frame_buffer[i])
                                captured_image = frame_buffer[i]
                                captured_roi = frame_roi_buffer[i]
                                captured = True
                                GPIO.output(GREEN_LED, GPIO.LOW)
                                read_plate.lcd_update("ANALYSING...")
                                break
                    else:
                        print("System not ready!")
                # USER INTERACTION>END


            # Get licence plate
            start=time.time()
            results = read_plate.get_plate(captured_image)
            end=time.time()
            print("Time to get plate: %f " %(end-start))
            print("Possible plates:")
            
            for item in results:
                print("  - "+item)

            # Image pre-processing
            img = illum_norm(captured_image)
            grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Identify logo
            start=time.time()
            logo = read_plate.logo_detector.detect(grey)
            end=time.time()

            # Extract colour and display results
            col_outputs = []
            if is_plate:
                avg_top, closest_col, alternate_cols = colour_extraction(captured_roi, img)
                print("")
                print("Detected color(region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
                print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
                print("Alternate colour 1: "+str(alternate_cols[0]))
                print("Alternate colour 2: "+str(alternate_cols[1]))
                col_outputs.append(closest_col)
                col_outputs.append(alternate_cols[0])
                col_outputs.append(alternate_cols[1])
            col_names = []
            for col in col_outputs:
                col_names.append(str(col[2]))
            print (col_names)
            print("")
            print("Vehicle make: " + logo)
            print("Time to detect logo: %f" % (end-start))

            print("")
            print ("GPS Mode: ", gpsd.fix_mode)
            print("Plate was captured at: ")

            gps_loc = (0.0,0.0)

            if gpsd.fix_mode != 1:
                gps_loc = gpsd.est_loc
                print ('Location: ', gps_loc)
            else:
                print("NO GPS FIX!")

            read_plate.lcd_update(str(results[0]))

            
            final_plate = ""
            lp_pos = 0
            while True:
                read_plate.lcd_update(str(results[lp_pos]))
                if (GPIO.input(MIDDLE_BTN)==1):
                    final_plate = str(results[lp_pos])
                    break
                elif (GPIO.input(RIGHT_BTN)==1):
                    if lp_pos == 2:
                        lp_pos = 0
                    else:
                        lp_pos += 1
                    time.sleep(0.1)
                elif (GPIO.input(LEFT_BTN)==1):
                    if lp_pos == 0:
                        lp_pos = 2
                    else:
                        lp_pos -= 1
                    time.sleep(0.1)
                else:
                    pass
        
            time.sleep(0.5)
            final_col_pos = 0
            final_colour = None
            while True:
                read_plate.lcd_update(str(col_names[final_col_pos]))
                if (GPIO.input(MIDDLE_BTN)==1):
                    break
                elif (GPIO.input(RIGHT_BTN)==1):
                    if final_col_pos == 2:
                        final_col_pos = 0
                    else:
                        final_col_pos += 1
                    time.sleep(0.1)
                elif (GPIO.input(LEFT_BTN)==1):
                    if final_col_pos == 0:
                        final_col_pos = 2
                    else:
                        final_col_pos -= 1
                    time.sleep(0.1)
                else:
                    pass
            
            if final_col_pos == 0:
                final_colour = closest_col
            elif final_col_pos == 1:
                final_colour = alternate_cols[0]
            else:
               final_colour = alternate_cols[1]
            
            print(final_plate)
            print(final_colour)
            fps.display_result(test_name="Main processing loop")
            break


    except KeyboardInterrupt:
            print("Main program closing...")
            read_plate.stop_stream()
            read_plate.lcd.clear()
            gpsd.stop()
            f = open(ALPR_BASE_DIR+"exit", 'w')
            f.close()
            GPIO.cleanup()
            sys.exit()

print("Main program closing...")
read_plate.stop_stream()
read_plate.lcd.clear()
gpsd.stop()
f = open(ALPR_BASE_DIR+"exit", 'w')
f.close()
#GPIO.cleanup()
sys.exit()
