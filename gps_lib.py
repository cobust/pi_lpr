import os, time, utm, math
from collections import deque
from gps import *
from threading import Thread

class GPSFeed():

    """
        Class that notifies  the GPSD daemon to watch the serial port,
        and captures the data received from the GPSS.

        Requires that GPSD daemon must already be running
    """

    def __init__(self):
        self.running = True
        self.stream = None

        # Relevant GPS data
        self.lat = None
        self.long = None
        self.lat_prev = None
        self.long_prev = None
        self.pos_at_stop = (None, None)
        self.alt = None
        self.fix_mode = None
        self.speed = None

        self.gdop = None
        self.pdop = None
        self.sat = None

        # Calculation variables
        self.est_loc = (None, None) # Lat, Long
        self.walking = False
        self.walk_secs = 0
        self.lat_hist = deque()
        self.long_hist = deque()
        self.dist = 0
        self.est_dist = 0
        self.angle = 0
        self.time_at_update = None
        self.time_at_prev = None
        self.calc_time = None
    
    def update(self):

        # Tells the daemon to watch the serial port for data
        self.stream = gps(mode=WATCH_ENABLE)
        
        while self.running:

            # If the service has not been stopped, pull the latest
            # data from the daemon's buffer
            self.stream.next()

            self.lat_prev = self.lat
            self.long_prev = self.long

            self.time_at_prev = self.time_at_update
            self.time_at_update = time.time()
            
            self.lat = self.stream.fix.latitude
            self.long = self.stream.fix.longitude

            self.alt = self.stream.fix.altitude
            self.fix_mode = self.stream.fix.mode
            self.speed = self.stream.fix.speed

            self.gdop = self.stream.gdop
            self.pdop = self.stream.pdop
            self.sat = self.stream.satellites

            start = time.time()

            # Attempt to better estimate location
            
            if isinstance(self.speed, float):
                #print("Speed is float")
                if self.speed <= 0.8:
                    self.walking = False
                else:
                    self.walking = True
                    

            # At startup, change location from None to current coords
            if self.est_loc[0] is None or self.est_loc[1] is None:
                self.est_loc = (self.lat, self.long)

            if isinstance(self.lat, float) and isinstance(self.long, float):
                #print("Lat and long are floats")
                #print(len(self.lat_hist), len(self.long_hist))
                if self.walking == False:
                    self.lat_hist.append(self.lat)
                    self.long_hist.append(self.long)
                    self.est_loc = (sum(self.lat_hist)/float(len(self.lat_hist)), sum(self.long_hist)/float(len(self.long_hist)))
                else:
                    # User started walking again
                    # Reset history
                    if len(self.lat_hist) > 0 or len(self.long_hist) > 0:
                        #print("Reset!")
                        self.lat_hist = deque()
                        self.long_hist = deque()

                    # Just for now
                    self.est_loc = (self.lat, self.long)

                if self.time_at_prev is not None and isinstance(self.speed, float) and self.walking:
                    delta_time = self.time_at_update - self.time_at_prev
                    self.est_dist = self.speed*delta_time #if delta_time > 0.1 else 0
                else:
                    self.est_dist = 0

                if isinstance(self.lat_prev, float) and isinstance(self.long_prev, float) and math.isnan(self.lat) == False and math.isnan(self.long) == False:
                    if self.lat != 0.0 and self.long != 0.0 and self.lat_prev != 0.0 and self.long_prev != 0.0:
                        #print(self.lat, self.long, type(self.lat), type(self.long))
                        east, north, zone, letter = utm.from_latlon(self.lat, self.long)

                        east_prev, north_prev, zone, letter = utm.from_latlon(self.lat_prev, self.long_prev)

                        aang = east-east_prev
                        teenoorst = north-north_prev

                        self.dist = math.sqrt(aang**2 + teenoorst**2)
                        self.angle = math.asin(teenoorst/self.dist) if self.dist > 0 else 0
                        #print("Angle before: ", self.angle)
                        self.angle = (-1)*self.angle if self.angle < 0 else self.angle

                        if self.est_dist > 0 and self.angle != 0:
                            #print("Angle: ", self.angle)
                            xd = self.est_dist*math.cos(self.angle)
                            yd = self.est_dist*math.sin(self.angle)
        
                            if aang > 0 and teenoorst > 0:
                                #Q1
                                pass
                            elif aang < 0 and teenoorst > 0:
                                #Q2
                                xd = -xd
                            elif aang < 0 and teenoorst < 0:
                                #Q3
                                xd = -xd
                                yd = -yd
                            else:
                                #Q4
                                yd = -yd

                            est_east = east + xd
                            est_north = north + yd

                            self.est_loc = utm.to_latlon(est_east, est_north, zone, letter)
                            self.pos_at_stop = self.est_loc

            else:
                # Device has no fix
                # Reset history
                self.lat_hist = deque()
                self.long_hist = deque()
                self.est_loc = (self.lat, self.long)

            # Ensure memory is not wasted
            # Only records past 120 seconds (120 * 64bit floats = approx 1MB of memory)
            # Also do not want to bugger the location if user is perhaps just moving super slow
            # Thus always just keep the freshest data
            
            if len(self.lat_hist) > 120 or len(self.long_hist) > 120:
                #print("Removed list items!")
                self.lat_hist.popleft()
                self.long_hist.popleft()
                #print(len(self.lat_hist), len(self.long_hist))

            end = time.time()
            self.calc_time = end-start


    def start(self):
        # Runs the 'update' method as a thread
        Thread(target=self.update, args=()).start()
        return self
        
    def stop(self):
        # Stops the requesting of data and tells the daemon
        # that it does not need to watch the serial port anymore
        self.running = False
        self.stream = gps(mode=WATCH_DISABLE)

    def get_loc(self):
        # Returns current estimated location
        return self.est_loc



# Temporary code to test current class
# TODO: Maybe start GPSD daemon in __init__?
if __name__ == '__main__':

    gpsd = GPSFeed().start()

    try:
        while True:
            os.system('clear')

            print ('GPS data')
            print ('---------------------------------------')
            print ('Latitude: ', gpsd.lat)
            print ('Longitude: ', gpsd.long)
            print ('Altitude: ', gpsd.alt)
            print ('Speed (m/s): ', gpsd.speed)
            print ('Mode: ', gpsd.fix_mode)
            print ('\nDebug data')
            print ('---------------------------------------')
            print ('PDOP: ', gpsd.pdop)
            print ('Estimated location: ', gpsd.est_loc)
            print ('Walking: ', gpsd.walking)
            print ('Distance ', gpsd.dist)
            print ('Angle: ', gpsd.angle)
            print ('Est dist:', gpsd.est_dist)

            time.sleep(1.1) # A bit longer than 1s to compensate for possible asyncronous behaviour from gps module
            #sudo gpsmon

    except KeyboardInterrupt:
        gpsd.stop()
        print('Stopped')
        

    
        
