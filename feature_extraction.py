import numpy as np
import webcolors
from plate_identifier import *
#import cv2, time, scipy, struct, scipy.cluster, colorsys
import cv2, time, struct, colorsys

# FOR THE COLOUR EXTRACTION FUNCTIONS
# Either img or img_dir must be provided, not both. If both are provided,
# img_dir will be used as the default

# ROI expected in format (x,y,w,h) where (x,y) is the top left corner
# of ROI, and (w,h) is the width and height of ROI

# roi is expected to contain only ONE ROI

def color_around_roi_testing(roi, img=None, img_dir=None):

    if img is None and img_dir is None:
        print("img or img_dir is not provided!")
        return
    if len(roi) == 0:
        print ("Empty ROI given.")
        return
    if img_dir is not None:
        img = cv2.imread(img_dir)
        
    if len(roi)<=1:
        x, y, w, h = roi[0]
    else:
        x, y, w, h = roi[1]
    X_ENLARGE = int(w/1.8)
    Y_ENLARGE = int(h*1.5)

    # Making sure that the cropping area is not out of bounds of
    # the image's resolution

    top_left_y = y-Y_ENLARGE if y-Y_ENLARGE > 0 else 0
    bot_y = y+h+Y_ENLARGE/2 if y+h+Y_ENLARGE/2 < img.shape[0] else img.shape[0]
    
    top_left_x = x-X_ENLARGE if x-X_ENLARGE > 0 else 0
    bot_x = x+w+X_ENLARGE if x+w+X_ENLARGE < img.shape[1] else img.shape[1]

    # Crop image
    crop_img = img[top_left_y:bot_y, top_left_x:bot_x]

    # Region above ROI
    above = img[top_left_y:y, top_left_x:bot_x]
    above_roi = [[top_left_x,top_left_y,bot_x-top_left_x,y-top_left_y]]

    # Region left of ROI
    left = img[y+1:y+h-1, top_left_x:x]
    left_roi = [[top_left_x, y, x-top_left_x,h]]

    # Regoin below ROI
    below = img[y+h:bot_y, top_left_x:bot_x]
    below_roi = [[top_left_x, y+h, bot_x-top_left_x, bot_y-(y+h)]]

    # Region right of ROI
    right = img[y+1:y+h-1, x+w:bot_x]
    right_roi = [[x+w, y, bot_x-(x+w), h]]
    
    avg_top = avg_colour(above)
    avg_bot = avg_colour(below)
    avg_right = avg_colour(right)
    avg_left = avg_colour(left)
    
    a = np.array([avg_top,avg_bot,avg_right,avg_left])/4
    tot = a.sum(axis=0)
    tot_rgb = [tot[2], tot[1], tot[0]]
    #print("Avg col of ROI minus Plate area: #" + rgb2hex(tot_rgb))
    tile_size = 100
    
    avg_col_tot = np.array([[tot]*tile_size]*tile_size, np.uint8)
    avg_col_top = np.array([[avg_top]*tile_size]*tile_size, np.uint8)
    
    avg_col = avg_colour(crop_img)
    avg_col_rgb = [avg_col[2], avg_col[1], avg_col[0]]
    #print("Avg col of entire ROI: #" + rgb2hex(avg_col_rgb))
    avg_col_img = np.array([[avg_col]*tile_size]*tile_size, np.uint8)


    # Generate images for report
    """
    cv2.imwrite("1to4.jpg", avg_col_tot)
    cv2.imwrite("1.jpg", avg_col_top)
    cv2.imwrite("1to5.jpg", avg_col_img)

    new_img = img
    new_img = draw_rect(above_roi, new_img)
    new_img = draw_rect(left_roi, new_img)
    new_img = draw_rect(below_roi, new_img)
    new_img = draw_rect(right_roi, new_img)
     
    cv2.imwrite("sections.jpg", new_img)
    new_img = draw_rect(roi, new_img)
    cv2.imwrite("sections_with_roi.jpg", new_img)
    """
    
    return avg_col_img, crop_img, above, left, below, right, avg_col_tot, avg_col_top, avg_top

def color_around_roi_top(roi, img=None, img_dir=None):

    if img is None and img_dir is None:
        print("img or img_dir is not provided!")
        return
    if len(roi) == 0:
        print ("Empty ROI given.")
        return
    if img_dir is not None:
        img = cv2.imread(img_dir)
        
    if len(roi)<=1:
        x, y, w, h = roi[0]
    else:
        x, y, w, h = roi[1]
        
    X_ENLARGE = int(w/1.8)
    Y_ENLARGE = int(h*1.5)

    # Making sure that the cropping area is not out of bounds of
    # the image's resolution

    top_left_y = y-Y_ENLARGE if y-Y_ENLARGE > 0 else 0
    
    top_left_x = x-X_ENLARGE if x-X_ENLARGE > 0 else 0
    bot_x = x+w+X_ENLARGE if x+w+X_ENLARGE < img.shape[1] else img.shape[1]
    """
    top_left_y = int(top_left_y)
    top_left_x = int(top_left_x)
    bot_x = int(bot_x)
    x = int(x)
    y = int(y)
    """

    # Region above ROI
    above = img[top_left_y:y, top_left_x:bot_x]

    avg_top = avg_colour(above)
    
    return avg_top

def clustering(opencv):
    N_CLUSTERS = 2

    start = time.time()
    im2 = cv2.cvtColor(opencv, cv2.COLOR_BGR2RGB)
    im2 = cv2.resize(im2,(150,150))
    im2 = im2.reshape(scipy.product(im2.shape[:2]), im2.shape[2])
    
    codes, dist = scipy.cluster.vq.kmeans(im2, N_CLUSTERS)
    vecs, dist = scipy.cluster.vq.vq(im2, codes)
    counts, bins = scipy.histogram(vecs, len(codes))
    index_max = scipy.argmax(counts)
    peak = codes[index_max]
    color = rgb2hex(peak)
    end = time.time()

    print("Opencv: \nMost frequent colour is %s (#%s)" % (peak, color))
    print("Total time: " + str(end-start))

    tile = np.array([[peak]*100]*100, np.uint8)
    tile = cv2.cvtColor(tile, cv2.COLOR_RGB2BGR)

    return tile

def illum_norm(img):
    lab_image = cv2.cvtColor(img, cv2.COLOR_BGR2Lab)
    l = lab_image[:,:,0]
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(10,10))
    cl1 = clahe.apply(l)
    lab_image[:,:,0] = cl1
    bgr_img = cv2.cvtColor(lab_image, cv2.COLOR_Lab2BGR)
    return bgr_img
    

def avg_colour(image):
    """
        Calculates the average colour of an image

        Params
        image: Image that average colour is calculated from

        Retruns
        avg_col: Average colour in RGB format
    """
    # Calculate average colour of each row, then calculate the average
    # colour of the result
    avg_per_row = np.average(image, axis=0)
    avg_col = np.uint8(np.average(avg_per_row, axis=0))

    return avg_col

def avg_col_of_image(img):
    avg_col = avg_colour(img)
    avg_col_img = np.array([[avg_col]*150]*150, np.uint8)
    return avg_col_img, rgb2hex(avg_col)

def rgb2hex(bgr):
    #return ''.join(chr(c) for c in bgr2rgb(bgr)).encode('hex')
    return webcolors.rgb_to_hex(bgr2rgb(bgr))

def bgr2rgb(bgr):
    return [bgr[2],bgr[1],bgr[0]]

def rgb2bgr(rgb):
    return (rgb[2],rgb[1],rgb[0])

def rgb2hsv(rgb, sys='rgb', raw=False):

    # sys='bgr' for BGR, sys='rgb' for rgb
    r, g, b = 255.0, 255.0, 255.0

    if sys=='bgr':
        r, g, b = rgb[2]/r, rgb[1]/g, rgb[0]/b
    elif sys=='rgb':
        r, g, b = rgb[0]/r, rgb[1]/g, rgb[2]/b
    else:
        print("Incorrect colourspace defined")
        return None, None, None
    
    h, s, v =  colorsys.rgb_to_hsv(r, g, b)

    if raw:
        return h, s, v
    else:
        h = round(360.0*h, 2)
        s = round(100.0*s, 2)
        v = round(100.0*v, 2)


def hsv2rgb(hsv, raw=True):

    if raw:
        return colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2])
    else:
        return colorsys.hsv_to_rgb(hsv[0]/360.0, hsv[1]/100.0, hsv[2]/100.0)


def create_tile(bgr_color, res=100):
    return np.array([[bgr_color]*res]*res, np.uint8)


def alternate_colors(rgb, space='bgr', raw=False):

    if space == 'bgr':
        rgb = bgr2rgb(rgb)

    h, s, v = rgb2hsv(rgb, raw=True)
    h1, s1, v1 = h, s, v
    h2, s2, v2 = h, s, v
    
    if s > 0.75:
        s1 = 0.183
        s2 = 0.3657
    elif s >= 0.5:
        s1 = 0.25
        s2 = 0.9
    elif s < 0.5:
        s1 = 1
        s2 = 0.65

    r1, g1, b1 = hsv2rgb((h, s1, v))
    r2, g2, b2 = hsv2rgb((h, s2, v))

    #print([h1, s1, v1], [h2, s2, v2])

    name1 = closest_color_name([r1*255, g1*255, b1*255], space='rgb')
    name2 = closest_color_name([r2*255, g2*255, b2*255], space='rgb')

    return name1, name2


def closest_color_name(rgb_color, space='bgr'):
    # space: Either 'rgb' or 'bgr'
    min_colors = {}
    
    if space == 'bgr':
        
        for key, name in webcolors.css3_hex_to_names.items():

            r, g, b = webcolors.hex_to_rgb(key)
            r_dist = (r - rgb_color[2])**2
            g_dist = (g - rgb_color[1])**2
            b_dist = (b - rgb_color[0])**2
            min_colors[(r_dist + g_dist + b_dist)] = name
            
    elif space == 'rgb':
        
        for key, name in webcolors.css3_hex_to_names.items():

            r, g, b = webcolors.hex_to_rgb(key)
            r_dist = (r - rgb_color[0])**2
            g_dist = (g - rgb_color[1])**2
            b_dist = (b - rgb_color[2])**2
            min_colors[(r_dist + g_dist + b_dist)] = name
            
    else:
        print("Incorrect color space specified.")
        return None
    
    closest_name=min_colors[min(min_colors.keys())]
    closest_hex_color = webcolors.name_to_hex(closest_name)
    closest_rgb_color = webcolors.name_to_rgb(closest_name)
    
    #print(closest_name)
    #print(closest_hex_color)
    #print(closest_rgb_color)
    
    return closest_hex_color, closest_rgb_color, closest_name

def colour_extraction(roi, img):
    avg_top = color_around_roi_top(roi=roi, img=img)
    closest_col = closest_color_name(avg_top)
    alternate_cols = alternate_colors(avg_top)

    """
    print("Detected color of top (region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
    print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
    print("Alternate colour 1: "+str(alternate_cols[0]))
    print("Alternate colour 2: "+str(alternate_cols[1]))

    cv2.imwrite("col_orig.jpg",create_tile(avg_top))
    cv2.imwrite("col_nearest.jpg",create_tile(rgb2bgr(closest_col[1])))
    cv2.imwrite("col1.jpg", create_tile(rgb2bgr(alternate_cols[0][1])))
    cv2.imwrite("col2.jpg", create_tile(rgb2bgr(alternate_cols[1][1])))
    """

    return avg_top, closest_col, alternate_cols
    

if __name__ == '__main__':
    
    cc = PlateIdentifier(xml="/home/kojak/raw_data/classifier_outputs/finals/plates_final.xml")
    
    #img_dir = "/home/pi/pi_lpr/img/cr_2.jpg"
    #img_dir2 = "//home/pi/raw_data/phonepics/cropped/06.jpg"

    img_dir = "/home/kojak/pi_lpr/img/cr_1.jpg"
    
    img = cv2.imread(img_dir)
    #cv2.imwrite("original.jpg", img)

    #img = cv2.resize(img, (850,480))
    
    new = illum_norm(img)
    is_plate, roi = cc.scan_image(new)
    #cv2.imwrite("original2.jpg", new)
    
    #orig_col, hex = avg_col_of_image(img)
    #cv2.imwrite("original_col.jpg", orig_col)
   
    """
    cv2.imshow("old", img)
    cv2.imshow("new", new)
    cv2.waitKey()
    """
    """
    start=time.time()
    avg_color, crop_img, above, left, below, right, avg_col_tot, col_top, avg_top = color_around_roi_testing(roi, img=img)
    
    closest_col = closest_color_name(avg_top)
    alternate_cols = alternate_colors(avg_top)
    end=time.time()

    print("Detected color of top (region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: #"+str(rgb2hex(avg_top)))
    print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
    print("Alternate colour 1: "+str(alternate_cols[0]))
    print("Alternate colour 2: "+str(alternate_cols[1]))
    print("Getting color details took %f seconds" % (end-start))
    """

    print("\nFinal method: ")
    start=time.time()
    colour_extraction(roi,img)
    end=time.time()
    print("\nFinal method of getting color details took %f seconds" % (end-start))

    #cluster_col = clustering(crop_img)
    """
    
    while (cv2.waitKey(1) % 0x100) != 27:
        cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("Avg color", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("Avg col tot", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("Avg col TOP", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("ROI crop", cv2.WINDOW_AUTOSIZE)
        #cv2.namedWindow("Clustering color", cv2.WINDOW_AUTOSIZE)
        cv2.imshow("Preview", draw_rect(image=img, roi=roi))
        cv2.imshow("Avg color", avg_color)
        #cv2.imshow("Clustering color", cluster_col)
        cv2.imshow("Avg col tot", avg_col_tot)
        cv2.imshow("Avg col TOP", col_top)
        cv2.imshow("ROI crop", crop_img)


    cv2.destroyWindow("Preview")
    cv2.destroyWindow("Avg col tot")
    cv2.destroyWindow("Avg col TOP")
    cv2.destroyWindow("Above")
    cv2.destroyWindow("Avg color")
    cv2.destroyWindow("ROI crop")
    #cv2.destroyWindow("Clustering color")
    cv2.waitKey(1)
    """

