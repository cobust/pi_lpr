from openalpr import Alpr
import time, sys

#@profile
def alpr():
    
    alpr = Alpr("eu", "/usr/local/share/openalpr/config/openalpr.defaults.conf", "/usr/local/share/openalpr/runtime_data")

    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit(1)

    alpr.set_top_n(3)
    alpr.set_default_region("md")

    start=time.time()
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")

    # Multiple requests for benchmarking purposes
    
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")
    results = alpr.recognize_file("/home/pi/pi_lpr/img/h786poj.jpg")
    end=time.time()
    print(end-start)
    
    
    i = 0
    for plate in results['results']:
        i += 1
        print("Plate #%d" % i)
        print("   %12s %12s" % ("Plate", "Confidence"))
        for candidate in plate['candidates']:
            prefix = "-"
            if candidate['matches_template']:
                prefix = "*"

            print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))

    print("Done")
    
    # Release memory. Get segmentation fault for some reason.
    # TODO: Still have to look into that ^^
    #alpr.unload()

alpr()

