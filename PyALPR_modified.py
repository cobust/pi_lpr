import json, shlex, subprocess, time, cProfile, re, pstats


class PlateReader:

    def __init__(self):
        #webcam subprocess args
        webcam_command = "fswebcam -r 320x240 --no-banner --quiet alpr.jpg"
        self.webcam_command_args = shlex.split(webcam_command)

        #alpr subprocess args
        alpr_command = "alpr -c eu -n 5 -j /home/pi/scr/alpr/get_base/testpic5.jpg"
        #alpr_command = "alpr -c eu -n 3 -j alpr.jpg" 
        self.alpr_command_args = shlex.split(alpr_command)

    
    def webcam_subprocess(self):
        return subprocess.Popen(self.webcam_command_args, stdout=subprocess.PIPE)

    @profile
    def alpr_subprocess(self):
        return subprocess.Popen(self.alpr_command_args, stdout=subprocess.PIPE)

    
    def alpr_json_results(self):
        self.webcam_subprocess().communicate()
        alpr_out, alpr_error = self.alpr_subprocess().communicate()

        if not alpr_error is None:
            return None, alpr_error
        elif "No license plates found." in alpr_out:
            return None, None

        try:
            return json.loads(alpr_out), None
        except ValueError, e:
            return None, e

    
    def read_plate(self):
        alpr_json, alpr_error = self.alpr_json_results()

        if not alpr_error is None:
            print "ALPR error, OR NO PLATES FOUND"
            print alpr_error
            return

        if alpr_json is None:
            print "No results!"
            return
        
        results = alpr_json["results"]

        ordinal = 0
        for result in results:
            candidates = result["candidates"]

            for candidate in candidates:
                ordinal += 1
                print "Guess {0:d}: {1:s} {2:.2f}%".format(ordinal, candidate["plate"], candidate["confidence"])
        
if __name__=="__main__":
    plate_reader = PlateReader()
    N = 10
    #time.clock()
    """
    
    tot = 0
    minimum = 0
    maximum = 0
    """
    for i in range(N):
        #start = time.time()
        plate_reader.read_plate()
        """
        end = time.time()
        elapsed = end - start
        if i == 0:
            minimum = elapsed
            maximum = elapsed
        else:
            if elapsed < minimum:
                minimum = elapsed
            if elapsed > maximum:
                maximum = elapsed
        tot += elapsed
        """
    #print end - start
    #print "Min: " + str(minimum) + "\nMax: " + str(maximum)
    #print "Average: " + str(tot/N)
    
    #print time.clock()
    
    """
    cProfile.run('plate_reader.read_plate()', 'restats')
    p = pstats.Stats('restats')
    p.sort_stats('time')
    p.print_stats()
    """
