from openalpr import Alpr
import json, shlex, subprocess, time, cProfile, re, pstats, sys

class LPR():

    def __init__(self):
        #plate image directory
        self.img_dir = "alpr_img.jpg"

        #test plate reference
        self.control = "OS802HN"
        self.n_correct = 0
        self.n_tests = 0
        
        #webcam subprocess args
        webcam_command = "fswebcam -r 320x240 -S 65 --no-banner --quiet " + self.img_dir
        self.webcam_command_args = shlex.split(webcam_command)

        #raspistill subprocess args 
        picam_command = "raspistill -w 640 -h 480 -t 1500 -n -o " + self.img_dir
        self.picam_command_args = shlex.split(picam_command)

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general   parameters
        self.alpr_n = 5   #amount of plates to return
        
    def init_alpr(self):
        print("Initializing OpenALPR...")
        start = time.time()
        self.alpr = Alpr(self.alpr_country, self.alpr_confdir, self.alpr_runtimedir)

        if not self.alpr.is_loaded():
            print("Error loading OpenALPR")
            sys.exit()

        self.alpr.set_top_n(self.alpr_n)
        self.alpr.set_default_region("md")
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n") 

    def webcam_subprocess(self):
        return subprocess.Popen(self.webcam_command_args, stdout=subprocess.PIPE)

    def picam_subprocess(self):
        return subprocess.Popen(self.picam_command_args, stdout=subprocess.PIPE)
    
    def take_pic(self):
        print("\nTaking picture...")
        start = time.time()      
        self.webcam_subprocess().communicate()  #take picture with webcam
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

    def picam_pic(self):
        print("\nTaking picture...")
        start = time.time()      
        self.picam_subprocess().communicate()  #take picture with webcam
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

    def get_plate(self):
        correct = False
        print("Analyzing image...")
        start = time.time()
        results = self.alpr.recognize_file(self.img_dir)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

        if results['results']:  #check if returned number plates is empty
            print("Results: ")
            i = 0
            for plate in results['results']:
                i += 1
                print("Plate #%d" % i)
                print("   %12s %12s" % ("Plate", "Confidence"))
                first_plate = str(plate['candidates'][0]['plate'])
                if first_plate == self.control:
                    correct = True
                    print("True!")
                for candidate in plate['candidates']:
                    prefix = "-"
                    if candidate['matches_template']:
                        prefix = "*"

                    print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
        else:
            print ("No plates were detected.")
            
        print ("Done.")
        return correct

#cProfile.run('alpr()', 'restats')
#p = pstats.Stats('restats')
#p.sort_stats('time')
#p.print_stats()
        
if __name__=="__main__":
    read_plate = LPR()
    read_plate.init_alpr()

    print("System ready.")
    while 1:
        keyb_in = raw_input("\n1. Analyze single image\n2. Loop analysis\n3. Exit\n")
        
        if keyb_in == '1':
            #read_plate.take_pic()
            read_plate.picam_pic()
            rand_var = read_plate.get_plate()
        elif keyb_in == '2':
            read_plate.n_tests = 0
            read_plate.n_correct = 0
            print("\nCtrl + C to end loop.")
            try:
                while True:
                    read_plate.n_tests += 1
                    #read_plate.take_pic()
                    read_plate.picam_pic()
                    time.sleep(0.1)
                    if read_plate.get_plate():
                        read_plate.n_correct += 1
                        
                    print ("\n" + str(read_plate.n_tests) + " tests.")
                    print (str(read_plate.n_correct) + " correct.")
                    print("Accuracy: " + str((read_plate.n_correct/read_plate.n_tests)*100) + "%")
            except KeyboardInterrupt:
                print("Stopped.")
        elif keyb_in == '3':
            read_plate.alpr.unload()  #release memory
            sys.exit()
        else:
            print("Incorrect input.")
