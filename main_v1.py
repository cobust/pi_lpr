from picamera import PiCamera
from openalpr import Alpr
from threading import Thread
from alpr_lib import *
from feature_extraction import *
from logo_identifier import *
from plate_identifier import *
from gps_lib import *
import json, shlex, subprocess, cv2, sys, time

# Implementing the API of OpenALPR

RESOLUTION = (640, 480) # Camera resolution
IMG_DIR = 'alpr.jpg'

class PlateReader:

    def __init__(self):

        #alpr configuration parameters
        self.alpr_country = "eu"
        self.alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
        self.alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory

        #alpr general parameters
        self.alpr_n = 5   #amount of plates to return


        print("Initializing GPS...")
        start = time.time()
        self.gps = GPSFeed().start()
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")


        # Initialise PiCam
        print("Initializing Camera...")
        start = time.time()
        self.camera = PiCamera()
        self.camera.resolution = RESOLUTION
        self.camera.start_preview()
        self.camera_started = True
        time.sleep(1.5)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

        print("Initializing classifiers...")
        start = time.time()

        # Licence plate classifier
        self.plate_detector = PlateIdentifier()
        self.logo_detector = LogoIdentifier()

        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")
    
    def capture_image(self):
        self.camera.capture(IMG_DIR)

    def init_alpr(self):
        print("Initializing OpenALPR...")
        start = time.time()
        # Instantiate OpenALPR
        self.alpr = Alpr(self.alpr_country, self.alpr_confdir, self.alpr_runtimedir)

        if not self.alpr.is_loaded():
            print("Error loading OpenALPR")
            sys.exit()

        self.alpr.set_top_n(self.alpr_n)
        self.alpr.set_default_region("md")
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

    def get_plate(self, img_dir):
        
        print("Analyzing image...")
        start = time.time()
        results = self.alpr.recognize_file(img_dir)
        end = time.time()
        print("Done. Took " + str(end - start) + " seconds.\n")

        if results['results']:  #check if returned number plates is empty

            print("Results: ")
            i = 0
            for plate in results['results']:
                i += 1
                print("Plate #%d" % i)
                print("   %12s %12s" % ("Plate", "Confidence"))
                first_plate = str(plate['candidates'][0]['plate'])
                for candidate in plate['candidates']:
                    prefix = "-"
                    if candidate['matches_template']:
                        prefix = "*"

                    print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
        else:
            print ("No plates were detected.")
            
        print ("Done.")

def get_logo():
    print("Running...")
    os.system("python logo_identifier2.py")
#@profile    
def run():
    plate_reader = PlateReader()
    plate_reader.init_alpr()

    for i in range(2):
        
        plate_reader.capture_image()
        plate_reader.get_plate(IMG_DIR)
        img = cv2.imread(IMG_DIR)
        img = illum_norm(img)
        grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        logo = plate_reader.logo_detector.detect(grey)
        is_plate, roi = plate_reader.plate_detector.scan_image(grey)

        if is_plate:
            avg_top, closest_col, alternate_cols = colour_extraction(roi, img)
            print("Detected color(region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
            print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
            print("Alternate colour 1: "+str(alternate_cols[0]))
            print("Alternate colour 2: "+str(alternate_cols[1]))
        
        print("Vehicle make: " + logo)
        
        print("Location: ", plate_reader.gps.est_loc)
        
    plate_reader.gps.stop()

run()
"""
if __name__ == '__main__':
    plate_reader = PlateReader()
    plate_reader.init_alpr()
    total_time = 0
    i = 0

    while True:
        keyb_in = raw_input("\n1. Analyze image\nAny other key - Exit\n")

        if keyb_in == '1':
            i+=1
            start = time.time()
            plate_reader.capture_image()
            plate_reader.get_plate(IMG_DIR)
            img = cv2.imread(IMG_DIR)
            img = illum_norm(img)
            is_plate, roi = plate_reader.plate_detector.scan_image(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))

            if is_plate:
                avg_top, closest_col, alternate_cols = colour_extraction(roi, img)
                print("Detected color(region 1): \n - RGB:" + str(bgr2rgb(avg_top))+"\n - Hex: "+str(rgb2hex(avg_top)))
                print("Closest color and name: "+str((closest_col[0], closest_col[1], closest_col[2])))
                print("Alternate colour 1: "+str(alternate_cols[0]))
                print("Alternate colour 2: "+str(alternate_cols[1]))
            logo = plate_reader.logo_detector.detect(img)
            
            end = time.time()
            total_time += end-start
            print("Vehicle make: " + logo)
            print("Location: ", plate_reader.gps.est_loc)
            print("Capture and analysis took %f seconds" % (end-start))
            
        else:
            print("Average time per capture and analysis: %f" % (total_time/i))
            print("Exiting ... ")
            plate_reader.gps.stop()
            break
"""
