from plate_identifier import *
from matplotlib import pyplot as plt
from openalpr import Alpr
import numpy as np
import glob, time, cv2, time, sys

#data_dir = "/home/kojak/raw_data/croatian_cars/*"
#data_dir = "/home/pi/raw_data/test/*"
#data_dir = "/media/kojak/Storage/Google Drive/Personal Docs/4de jaar/Semester 2/Skripsie/other/training_images/cars_bg_caltech/*"
#data_dir = "/home/kojak/raw_data/raw-neg/*"
#data_dir = "/home/pi/raw_data/phonepics/VW/*"
data_dir = "/home/pi/raw_data/croatian_cars/*.jpg"
file_list = glob.glob(data_dir)
n_files = len(file_list)

def single_logo():
    cascade_file = "cascade.xml"
    classifier = cv2.CascadeClassifier()
    classifier.load(cascade_file)

    img = cv2.imread("/home/pi/pi_lpr/img/P6040036.jpg")
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    results = classifier.detectMultiScale(gray_img, 1.3, 1)

    if len(results) != 0:
        cv2.imshow("out", draw_rect(results,img))
        cv2.waitKey()
    else:
        cv2.imshow("out", img)
        cv2.waitKey()

def bench_classifier():
    detector = PlateIdentifier(xml="/home/kojak/raw_data/classifier_outputs/finals/plates_final.xml")
    detector2 = PlateIdentifier(xml="/usr/share/openalpr/runtime_data/region/eu.xml")
    strict = 1
    scale = 1.3
    f = open("cascade_results_false_hit.txt", "a")
    f.write("Hit rate, strict, %f scale\n"%scale)

    try:
        while strict < 7:
            print(scale)
            hits = 0
            hits2 = 0
            total_time = 0
            for path, count in zip(file_list, range(n_files)):
                
                print(count, " of ", n_files)
                
                img = cv2.imread(path)
                start = time.time()
                is_plate, roi = detector.scan_image(change_clahe(img), scale=scale, strict=strict)
                is_plate2, roi2 = detector2.scan_image(change_clahe(img), scale=scale, strict=strict)
                end = time.time()
                total_time += end-start
                
                if is_plate:
                    hits += 1
                if is_plate2:
                    hits2 += 1
                    

                """
                if is_plate:
                    cv2.imshow("out", draw_rect(roi,img))
                else:
                    cv2.imshow("out", img)

                if is_plate2:
                    cv2.imshow("out2", draw_rect(roi2,img))
                else:
                    cv2.imshow("out2", img)
                cv2.waitKey()
                """
                    
                    
            write_str = "Openalpr: %.2f, %.4f, %f\n" % (hits2/n_files*100, total_time/n_files, strict)
            print(write_str)
            f.write(write_str)
            write_str = "own: %.2f, %.4f, %f\n" % (hits/n_files*100, total_time/n_files, strict)
            print(write_str)
            f.write(write_str)
            strict += 1

        f.close()
    except KeyboardInterrupt:
        print("Ended.")

def logo_identifier():
    #cascade_file = "/home/kojak/raw_data/classifier_outputs/VW/cascade1.xml"
    cascade_file = '/home/pi/pi_lpr/cascade_xml/logos/vw_cascade.xml'
    classifier = cv2.CascadeClassifier()
    classifier.load(cascade_file)
    
    true_hits = 0
    false_hits = 0

    pos = glob.glob("/home/pi/raw_data/z/*")
    negative_qty = n_files - len(pos)

    if classifier.empty():
        print("Error loading xml file")
        
    pos_split = []
    for item in pos:
        temp = item.split("/")
        pos_split.append(temp[len(temp)-1])

    scale = 1.0
    for i in range(1,2):

        scale += 0.1
        print("Scale: %f" %(scale))
        true_hits = 0
        false_hits = 0
        total_time = 0
        strict = 22

        if i == 1:
            strict = 29

        if i == 2:
            strict = 14

        if i == 3:
            strict = 8

        if i > 3:
            print("Scale larger than 1.3. Exiting...")
            sys.exit()
        
        for path, i in zip(file_list, range(n_files)):
            #print ("Strict ", strict, i, " out of ", n_files)
            img = cv2.imread(path)
            img = cv2.resize(img, (640, 480))
            #gray_img = change_clahe(img)
            gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            #th, imgt1 = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY);
            #th = cv2.adaptiveThreshold(gray_img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
            start=time.time()
            results = classifier.detectMultiScale(gray_img, 1.2, 14)
            end=time.time()
            print(end-start)

            if len(results) != 0:
                path_split = path.split("/")
                if path_split[len(path_split)-1] in pos_split:
                    true_hits += 1
                    #cv2.imshow("Positive", draw_rect(results, img))
                    #cv2.waitKey()
                    total_time += end-start
                else:
                    false_hits += 1
                    #cv2.imshow("Negative", draw_rect(results, img))
                    #cv2.waitKey()
                    #print(path)
                    
        print("False hits: %d out of %d. False hit rate: %f percent" % (false_hits, negative_qty, false_hits/negative_qty*100))
        print("True hits: %d out of %d. True hit rate: %f percent" % (true_hits, len(pos), true_hits/len(pos)*100))

        #f = open("logo_classifier_bench.txt", 'a')
        #out_str = "%d, %f, %f\n" % (strict, false_hits/negative_qty*100, true_hits/len(pos)*100)
        out_str = "%d, %f, %f\n" % (strict, scale, total_time/true_hits)
        #f.write(out_str)
        print(out_str)
        #f.close()

    """
    f = open("/home/kojak/pi_lpr/benchmarking/logo_identifier_bench/results.txt", 'a')
    out_str = "Cropped logos, 0.995 hit param, 0.45 false detect, 1.3 scale, 1 strict\nFalse hit rate: %f percent\nTrue hit rate: %f percent\n\n" %(false_hits/negative_qty*100, true_hits/len(pos)*100)
    f.write(out_str)
    f.close()
    """

def bench_logo_identifier():

    #cascade_file = "/home/kojak/raw_data/classifier_outputs/VW/cascade1.xml"
    cascade_file = 'cascade.xml'
    classifier = cv2.CascadeClassifier()
    classifier.load(cascade_file)
    
    hits = 0

    if classifier.empty():
        print("Error loading xml file")
        sys.exit()
        
    for path, i in zip(file_list, range(n_files)):

        print (i, " out of ", n_files)
        img = cv2.imread(path)
        img = cv2.resize(img, (img.shape[0]/5, img.shape[1]/5))
        print(img.shape)
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        results = classifier.detectMultiScale(gray_img, 1.3, 1)

        if len(results) != 0:
            hits += 1
            
    print("Hitrate: %d out of %d. %f percent" % (hits, n_files, hits/n_files*100.0))


def bench_alpr():
    alpr_country = "eu"
    alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
    alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory
    alpr_n = 5   #amount of plates to return
    img_dir = "/home/pi/pi_lpr/img/cr_4.jpg"
    control = "171NVX75"

    print("Initializing OpenALPR...")
    start = time.time()
    alpr = Alpr(alpr_country, alpr_confdir, alpr_runtimedir)

    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit()

    alpr.set_top_n(alpr_n)
    alpr.set_default_region("cr")
    end = time.time()
    print("Done. Took " + str(end - start) + " seconds.\n")

    paths = []
    plates = []
    with open("croatian_plates_bench.txt", 'r') as f:
        for line in f:
            split = line.split(",")
            paths.append(split[0])
            plates.append(split[1])
    f.close()

    for i in range(len(plates)):
        plates[i] = plates[i].rstrip()

    count = 0
    missed = 0
    correct = 0
    total_time = 0
    missed_plates = []
    
    detector = PlateIdentifier(xml="/home/pi/openalpr/runtime_data/region/eu.xml")
    
    try:
        for path in file_list:

            count += 1
            print(count, " of ", n_files)

            start = time.time()
            #img = cv2.imread(path)
            #cv2.imwrite("out.jpg", change_clahe(img))
            
            img = cv2.imread(path)
            grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            is_plate, roi = detector.scan_image(grey, scale=1.1, strict=15)
            cropped = img
            if is_plate:
                cropped = get_plate_roi(img, roi)
            cv2.imwrite("cropped.jpg", cropped)
                #cv2.imshow("test", cropped)
                #cv2.waitKey()
            
            results = alpr.recognize_file("cropped.jpg")
            end = time.time()
            #identified_plate = get_first_plate(results)
            identified_plate = get_best_plate(results)
            total_time += end-start

            pos_index = paths.index(path)
            print(identified_plate, plates[pos_index])

            if identified_plate is None:
                missed_plates.append(path)
                missed += 1
            elif identified_plate == plates[pos_index]:
                print("Match!")
                correct += 1

        print("Average time per plate: %f" % (total_time/n_files))
        print("\nMisses: " + str(missed))
        print("\nCorrect: %d" % (correct))
        print("\nTotal files: " + str(n_files))
        f = open("missed_plates.txt", 'w')
        for item in missed_plates:
            f.write(item+"\n")
        f.close()
        

    except KeyboardInterrupt:
        print("Ended.")

def set_metadata():
    alpr_country = "eu"
    alpr_confdir = "/usr/local/share/openalpr/config/openalpr.defaults.conf"  #opelalpr config file location
    alpr_runtimedir = "/usr/local/share/openalpr/runtime_data"  #openalpr runtime_data directory
    alpr_n = 2   #amount of plates to return

    #img_dir = "/home/pi/raw_data/croatian_cars/P1010001.jpg"
    #control = "171NVX75"

    print("Initializing OpenALPR...")
    start = time.time()
    alpr = Alpr(alpr_country, alpr_confdir, alpr_runtimedir)

    if not alpr.is_loaded():
        print("Error loading OpenALPR")
        sys.exit()

    alpr.set_top_n(alpr_n)
    alpr.set_default_region("md")
    end = time.time()
    print("Done. Took " + str(end - start) + " seconds.\n")

    count = 0
    total_time = 0
    finished_images = []

    with open("croatian_plates.txt", 'r') as f:
        for line in f:
            finished_images.append(line.split(",")[0])
    f.close()

    try:
        for path in file_list:
            count += 1
            print(count, " of ", n_files)
            if path not in finished_images:

                results = alpr.recognize_file(path)
                identified_plate = get_first_plate(results)

                print("Identified plate as: " + str(identified_plate))
                print("Is this correct? (y/n)")
                f = open("croatian_plates.txt", 'a')
                f.write(path)
                img = cv2.imread(path)
                
                while True:
                    
                    cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
                    cv2.imshow("Preview", img)
                    c = cv2.waitKey(1) % 100

                    #if c != 99:
                        #print(c)

                    if c == 21: #yes
                        f.write(","+str(identified_plate)+"\n")
                        break
                    if c == 10: #no
                        correct_plate = raw_input("Enter correct value: ")
                        f.write(","+correct_plate+"\n")
                        break
                    
                f.close()
                cv2.destroyWindow("Preview")
                cv2.waitKey(1)
            else:
                print("Already in file.")
        


    except KeyboardInterrupt:
        f.close()
        print("Ended.")

def correct_metadata():

    count = 0

    paths = []
    plates = []
    with open("croatian_plates.txt", 'r') as f:
        for line in f:
            split = line.split(",")
            paths.append(split[0])
            plates.append(split[1])
    f.close()

    for i in range(len(plates)):
        plates[i] = plates[i].rstrip()

    try:
        for path in file_list:

            count += 1
            print(count, " of ", n_files)
            
            pos_index = paths.index(path)
            print("Identified plate as: " + plates[pos_index])
            print("Is this correct? (y/n)")
            
            f = open("croatian_plates_bench.txt", 'a')
            f.write(path)
            img = cv2.imread(path)
            
            while True:
                
                cv2.namedWindow("Preview", cv2.WINDOW_AUTOSIZE)
                cv2.imshow("Preview", img)
                c = cv2.waitKey(1) % 100


                if c == 21: #yes
                    f.write(","+plates[pos_index]+"\n")
                    break
                if c == 10: #no
                    correct_plate = raw_input("Enter correct value: ")
                    f.write(","+correct_plate+"\n")
                    break
                
            f.close()
            cv2.destroyWindow("Preview")
            cv2.waitKey(1)
        
    except KeyboardInterrupt:
        f.close()
        print("Ended.")

        
def get_plate_roi(img, roi):
    large = False
    if len(roi)<=1:
        x, y, w, h = roi[0]
    else:
        x, y, w, h = get_largest_roi(roi)
        large = True
    
    X_ENLARGE = int(w*0.13)
    Y_ENLARGE = int(h*0.13)

    # Making sure that the cropping area is not out of bounds of
    # the image's resolution

    top_left_y = y-Y_ENLARGE if y-Y_ENLARGE > 0 else 0
    bot_y = y+h+Y_ENLARGE/2 if y+h+Y_ENLARGE/2 < img.shape[0] else img.shape[0]
    
    top_left_x = x-X_ENLARGE if x-X_ENLARGE > 0 else 0
    bot_x = x+w+X_ENLARGE if x+w+X_ENLARGE < img.shape[1] else img.shape[1]

    # Crop image
    crop_img = img[top_left_y:bot_y, top_left_x:bot_x]
    return crop_img

def get_largest_roi(roi):
    areas = []
    for x, y, w, h in roi:
        areas.append(w*h)

    largest = roi[areas.index(max(areas))]
    print (areas, largest)
           

    return largest
        

def get_first_plate(results):

    if results['results']:  #check if returned number plates is empty
        plates = results['results'][0]
        first_plate = str(plates['candidates'][0]['plate'])
        return first_plate
    else:
        return None

def get_best_plate(results):
    best_plate = None
    if results['results']:  #check if returned number plates is empty
        all_plates = results['results'][0]

        # Choose the plate that has the highest probability and matches the region
        for candidate in all_plates['candidates']:
            if candidate['matches_template']:
                best_plate = str(candidate['plate'])
                break

        # If no plates matches pattern, choose one with highest probability
        if best_plate is None:
            best_plate = str(all_plates['candidates'][0]['plate'])

    return best_plate

def print_plate_results(results):
    if results['results']:  #check if returned number plates is empty
        print("Results: ")
        i = 0
        for plate in results['results']:
            i += 1
            print("Plate #%d" % i)
            print("   %12s %12s" % ("Plate", "Confidence"))
            first_plate = str(plate['candidates'][0]['plate'])

            for candidate in plate['candidates']:
                prefix = "-"
                if candidate['matches_template']:
                    prefix = "*"

                print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))
        return first_plate
    else:
        print ("No plates were detected.")
        return None

def draw_hist():
    import struct, scipy, scipy.misc, scipy.cluster
    from PIL import Image

    N_CLUSTERS = 5

    for i in range(0,1):

        # OpenCV implementation
        start = time.time()
        opencv = cv2.imread("/home/pi/pi_lpr/img/ea7the.jpg")
        im2 = cv2.cvtColor(opencv, cv2.COLOR_BGR2RGB)
        im2 = cv2.resize(im2,(150,150))
        im2 = im2.reshape(scipy.product(im2.shape[:2]), im2.shape[2])

        codes, dist = scipy.cluster.vq.kmeans(im2, N_CLUSTERS)
        vecs, dist = scipy.cluster.vq.vq(im2, codes)
        counts, bins = scipy.histogram(vecs, len(codes))

        index_max = scipy.argmax(counts)
        peak = codes[index_max]
        color = ''.join(chr(c) for c in peak).encode('hex')
        end = time.time()

        print("Opencv: \nMost frequent colour is %s (#%s)" % (peak, color))
        print("Total time: " + str(end-start))

        tile1 = np.array([[peak]*300]*300, np.uint8)
        tile1 = cv2.cvtColor(tile1, cv2.COLOR_RGB2BGR)

        # PIL implementation
        start = time.time()
        im = Image.open("/home/pi/pi_lpr/img/ea7the.jpg")
        im = im.resize((150,150))
        ar = scipy.misc.fromimage(im)
        shape = ar.shape

        ar = ar.reshape(scipy.product(shape[:2]), shape[2])

        codes, dist = scipy.cluster.vq.kmeans(ar, N_CLUSTERS)

        vecs, dist = scipy.cluster.vq.vq(ar, codes)
        counts, bins = scipy.histogram(vecs, len(codes))

        index_max = scipy.argmax(counts)
        peak = codes[index_max]
        color = ''.join(chr(c) for c in peak).encode('hex')
        end = time.time()
        tile2 = np.array([[peak]*300]*300, np.uint8)
        tile2 = cv2.cvtColor(tile2, cv2.COLOR_RGB2BGR)


        print("\nPIL:\nMost frequent colour is %s (#%s)" % (peak, color))
        print("Total time: " + str(end-start))


        while (cv2.waitKey(1) % 0x100) != 27:
            cv2.namedWindow("OpenCV", cv2.WINDOW_AUTOSIZE)
            cv2.namedWindow("PIL", cv2.WINDOW_AUTOSIZE)
            cv2.namedWindow("Original", cv2.WINDOW_AUTOSIZE)
            cv2.imshow("OpenCV", tile1)
            cv2.imshow("Original", opencv)
            cv2.imshow("PIL", tile2)
        cv2.destroyWindow("OpenCV")
        cv2.destroyWindow("Original")
        cv2.destroyWindow("PIL")
        cv2.waitKey(1)

def clahe(img):
    #img = cv2.imread("/home/pi/pi_lpr/img/cr_4.jpg")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    cl1 = clahe.apply(gray)
    """
    while (cv2.waitKey(1) % 0x100) != 27:
        cv2.namedWindow("CLAHE", cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow("Original", cv2.WINDOW_AUTOSIZE)
        cv2.imshow("CLAHE", cl1)
        cv2.imshow("Original", gray)
    cv2.destroyWindow("CLAHE")
    cv2.destroyWindow("Original")
    cv2.waitKey(1)
    """
    return cl1

def change_clahe(img, cl=2.0, tile=(10,10)):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=cl, tileGridSize=tile)
    cl1 = clahe.apply(gray)
    return cl1

def view_clahe():
    img = cv2.imread("/home/pi/pi_lpr/img/cr_4.jpg")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite("original.jpg", gray)

    """
    while True:
        fastest = 10.0
        fastest_str = ""
        for i in range(2, 33):
            start = time.time()
            cl1 = change_clahe(img, tile=(i,i))
            end  = time.time()
            if end-start < fastest:
                fastest = end-start
                fastest_str = "%dx%d" % (i,i)
            #cv2.imwrite("%dx%d.jpg"%(i,i), cl1)

            while (cv2.waitKey(1) % 0x100) != 27:
                cv2.namedWindow("CLAHE", cv2.WINDOW_AUTOSIZE)
                cv2.namedWindow("Original", cv2.WINDOW_AUTOSIZE)
                cv2.imshow("CLAHE", cl1)
                cv2.imshow("Original", gray)
            cv2.destroyWindow("CLAHE")
            cv2.destroyWindow("Original")
            cv2.waitKey(1)

        print ("Fastest: "+fastest_str+" with %f seconds" % fastest)
    """
    cl1 = change_clahe(img, tile=(2,2))
    cv2.imwrite("2x2.jpg", cl1)
    cl1 = change_clahe(img, tile=(10,10))
    cv2.imwrite("10x10.jpg", cl1)
    th = cv2.adaptiveThreshold(cl1,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    cv2.imwrite("10x10thresh.jpg", th)

def illum_norm(img):

    lab_image = cv2.cvtColor(img, cv2.COLOR_BGR2Lab)
    l = lab_image[:,:,0]
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(10,10))
    cl1 = clahe.apply(l)
    lab_image[:,:,0] = cl1
    bgr_img = cv2.cvtColor(lab_image, cv2.COLOR_Lab2BGR)

    return cv2.cvtColor(bgr_img, cv2.COLOR_BGR2GRAY)

def car_logo_extraction():
    start = time.time()
    MIN_MATCH_COUNT = 10 # Minimum amount of matches needed to find the object
    FLANN_INDEX_KDTREE = 0
    #FLANN_INDEX_LSH = 6
    #GOOD_MATCH_RATIO = 0.7
    #n_FEATURES = 1000

    #cv2.ocl.setUseOpenCL(False)

    # Load images
    img1 = cv2.imread("/home/pi/raw_data/phonepics/VW/1.jpg", 0) # Query image image
    img2 = cv2.imread("/home/pi/raw_data/phonepics/VW/20161005_110843.jpg", 0) # train image

    #img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2 = cv2.resize(img2, (img2.shape[1]/5, img2.shape[0]/5))
    #img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # Calculate descriptors and keypoints
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)

    # Specifying algorithm for FLANN matcher
    """
    # ORB specific
    flann_params = dict(algorithm = FLANN_INDEX_LSH,
                        table_number = 6,      #12
                        key_size = 12,         #20
                        multi_probe_level = 1) #2
    """
    flann_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks = 50)

    # Initiate FLANN based matcher
    flann = cv2.FlannBasedMatcher(flann_params, search_params)

    # Find matches (2 match-lines for each keypoint since k = 2)
    matches = flann.knnMatch(des1, des2, k=2)

    """

    # This part is to show all the keypoints and map the matches
    matchesMask = [[0,0] for i in xrange(len(matches))]

    for i,(m,n) in enumerate(matches):
        if m.distance < GOOD_MATCH_RATIO*n.distance:
            matchesMask[i]=[1,0]

    draw_params = dict(matchColor = (0,255,0),
                       singlePointColor = (255,0,0),
                       matchesMask = matchesMask,
                       flags = 0)
    img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,matches,None,**draw_params)
    plt.imshow(img3,),plt.show()

    # This part is to detect an matched object
    """
    """
    # Extract the 'good' matches
    good = []
    for match in matches:
        if len(match) >= 2:
            m = match[0]
            n = match[1]
            if m.distance < GOOD_MATCH_RATIO*n.distance:
                good.append(m)
    """
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    # Extract locations of matched keypoints if enough matches were found
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()

        h, w = img1.shape
        pts = np.float32([[0,0], [0,h-1], [w-1,h-1], [w-1, 0]]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)

        img2 = cv2.polylines(img2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)

    else:
        print("Not enough matches found. %d/%d" %(len(good), MIN_MATCH_COUNT))
        matchesMask = None
    end = time.time()
    draw_params = dict(matchColor = (0,255,0),
                       singlePointColor = (255,0,0),
                       matchesMask = matchesMask,
                       flags = 2)
    img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    plt.imshow(img3, 'gray'),plt.show()
    print("Operation took %f seconds" %(end-start))
    

#bench_classifier()
bench_alpr()
#set_metadata()
#correct_metadata()
#draw_hist()
#clahe()
#quick()
#view_clahe()
#illum_norm(cv2.imread("/home/pi/pi_lpr/img/cr_4.jpg"))
#car_logo_extraction()
#logo_identifier()
#single_logo()
#bench_logo_identifier()
