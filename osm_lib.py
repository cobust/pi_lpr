import os, time, urllib.request, urllib.error
from threading import Thread
from osmapi import OsmApi


def internet_on():
    try:
        response=urllib.request.urlopen('http://www.google.co.za', timeout=1)
        return True
    except urllib.error.URLError:
        return False

class OSMHandler():

    def __init__(self):

        # Create connection if connected

        if internet_on():
            self.api = OsmApi(username="ctruter", password = u"pilpr123")
            self.current_changeset = self.api.ChangesetCreate({u'comment': u'RPi LPR Test nodes'})
        else:
            print("No internet connection! Cannot connect to OSM database.")
            self.api = None
        #self.api = OsmApi(api="api06.dev.openstreetmap.org", password = u"*******")
        #self.api = OsmApi(api="api06.dev.openstreetmap.org", passwordfile="/tmp/osm.password")

        self.latest_created_node = None
        self.latest_node_id = None
        

    def end_session(self):
        self.api.ChangesetClose()
        

    def re_connect(self):
        print("Attempting to reconnect...")
        
        if internet_on():
            print("Connected!")
            self.api = OsmApi(username="ctruter", password = u"pilpr123")
            self.current_changeset = self.api.ChangesetCreate({u'comment': u'RPi LPR Test nodes'})
            return True
        else:
            print("No internet connection! Cannot connect to OSM database.")
            self.api = None
            return False


    def add_node(self, lat, long, licence_plate, car_brand='unknown', color='unknown', parking_bay='unknown', img_url='none'):

        """
        test_data = { u"lat": 28.491903, u"lon": 77.0938, u"tag": { u"amenity": u"atm", u"name": u""
            +"Central Bank ATM",  u"operator": u"Central Bank of India", u"opening_hours": u"24/7",  u"fee": u"some", u""
                +"cash-in": u"no", u"drive-through": u"no", u"currency:INR": u"yes", u"language:en": u"yes", u"language:hi": u"yes" } }
        """

        data = { u"lat": lat, u"lon": long, u"tag": { u"id_tag": u"pi_lpr", u"plate": u""+licence_plate, u"car_brand": u""
            +car_brand,  u"car_color": u""+color, u"parking_bay_no": u""+parking_bay,  u"image_link": u""+img_url} }
        
        
        self.latest_created_node = self.api.NodeCreate(data)
        self.latest_node_id = int(self.latest_created_node['id'])
        print("Node created. ", self.latest_created_node)

    def prep_node(self, lat, long, licence_plate, car_brand='unknown', color='unknown', parking_bay='unknown', img_url='none'):
        data = { u"lat": lat, u"lon": long, u"tag": { u"plate": u""+licence_plate, u"car_brand": u""
            +car_brand,  u"car_color": u""+color, u"parking_bay_no": u""+parking_bay,  u"image_link": u""+img_url} }
        print("OSM node that will be added: ")
        print(data)

    def get_node(self, node_no):
        return self.api.NodeGet(node_no)

    def delete_node(self, node_no):
        print("Deleting node %d" % node_no)
        self.api.NodeDelete(self.get_node(node_no))
        print("Deleted.")
        

test = OSMHandler()
#print(type(test.get_node(4431448289 )))
#test.add_node(lat=-33.927264, long=18.865221, licence_plate="TEST123", color="#12ffee", parking_bay="12A")
#print(test.latest_created_node)
#test.delete_node(node_no=4431325890)
#test.delete_node(node_no=4431335290)
test.end_session()
