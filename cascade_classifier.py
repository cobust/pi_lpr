import numpy as np
from FPS import FPS
from benchmarking import change_clahe
from threading import Thread
from picam_thread import PiCamStream
from picamera import PiCamera
from picamera.array import PiRGBArray
import cv2, time, gc

#@profile
def plate_detector():
    stream = PiCamStream().start()
    time.sleep(1)
    eu_classifier = cv2.CascadeClassifier('/home/pi/openalpr/runtime_data/region/eu.xml')

    fps = FPS().start()

    while True:
        try:
            #img = cv2.imread('alpr_img.jpg')
            img = stream.get_frame()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
            plates = eu_classifier.detectMultiScale(change_clahe(gray), 1.32, 1)

            for (x,y,w,h) in plates:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

            
            cv2.imshow('img',img)
            
            c = cv2.waitKey(1) % 0x100
            if c == 27 or c == 10:
                stream.stop()
                break
            
            fps.count_frame()
        except KeyboardInterrupt:
            print ("Ended. ")
            stream.stop()
            break
        
    cv2.destroyAllWindows()
    fps.stop()
    fps.display_result()
    #gc.collect()

plate_detector()
