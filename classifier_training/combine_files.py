import numpy as np
import glob, time, cv2, os

file_list = glob.glob("/home/kojak/raw_data/classifier_outputs/Ford/*.info")
n_files = len(file_list)

new_file = open("ford.info", "w")

for path in file_list:
    with open(path, "r") as f:
        for line in f:
            new_file.write(line)
    
