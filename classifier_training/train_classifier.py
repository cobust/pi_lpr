import numpy as np
import glob, time, cv2, os

# ----- PARAMETERS ---------

# Make sure the following directories contain ONLY images (opencv compatible images)
# NB: Add an '*' to the end of the directories as shown below
POS_SAMPLES_DIR = "/home/kojak/raw_data/logos/TrainingData/VW/z/*"
NEG_SAMPLES_DIR = "/home/kojak/raw_data/raw-neg/*"

# The .xml cascade file and other outputs created by the script will be output in the directory below
BASE_OUTPUT_DIR = "/home/kojak/raw_data/classifier_outputs/Ford/"

# Desired name of the cascade .xml file. INCLUDE ".xml" as a prefix!
XML_FILENAME = "ford.xml"

# Desired name of the .vec .info and background files
VEC_FILENAME = "ford.vec"
INFO_FILENAME = "ford.info"
BG_FILENAME = "bg.txt"

# Amount of threads to use during training.
THREADS = 4

# width and height of the samples that will be created by opencv_createsamples in the .vec file
# Must have the same aspect ratio as your training images
# Use the resize_images() function to make sure your training images all have the same size/aspect ratio
# Image with area of 650px works best
SAMPLE_WIDTH = 35 # 52
SAMPLE_HEIGHT = 20 # 13

# Amount of stages to train
NUM_STAGES = 20

# Amount of samples to generate from single image
N_SAMPLES = 10080

# Minimal desired hit rate for each stage of the classifier. Overall hit rate may be estimated as (MIN_HITRATE\^NUM_STAGES)
# eg 20 stages and 0.9999 hit rate will yield an overall hit rate of 0.9999^20 = 0.998
MIN_HITRATE = 0.995 # 0.995 default in opencv

# Maximal desired false alarm rate for each stage of the classifier. Overall false alarm rate may be estimated as (MAX_FALSE_ALARM_RATE\^NUM_STAGES)
MAX_FALSE_ALARM_RATE = 0.45 # 0.5 default in opencv

# ----- END PARAMETERS ---------


# Create directory
if not os.path.exists(BASE_OUTPUT_DIR):
    os.makedirs(BASE_OUTPUT_DIR)

# Creating variables for script
vec_dir = BASE_OUTPUT_DIR+VEC_FILENAME
info_dir = BASE_OUTPUT_DIR+INFO_FILENAME
bg_dir = BASE_OUTPUT_DIR+BG_FILENAME
xml_dir = BASE_OUTPUT_DIR+XML_FILENAME

POS_SAMPLE_LIST = glob.glob(POS_SAMPLES_DIR)
N_POS_SAMPLES = len(POS_SAMPLE_LIST)

NEG_SAMPLE_LIST = glob.glob(NEG_SAMPLES_DIR)
N_NEG_SAMPLES = len(NEG_SAMPLE_LIST)


def resize_images(img_dir,w,h):
        sample_list = glob.glob(img_dir)
        n_samples = len(sample_list)

        count = 0
        for path in sample_list:
                print ("%d / %d" %(count, n_samples))
                count += 1
                img = cv2.imread(path)
                resized_image = cv2.resize(img, (w, h)) 
                output_img = "/home/kojak/raw_data/logos/TrainingData/VW/resized/"+str(count)+".jpg"
                cv2.imwrite(output_img,resized_image)

        print("Done.")
        
def change_clahe():
    img_dir = "/home/kojak/raw_data/logos/TrainingData/VW/own/*"
    out_dir = "/home/kojak/raw_data/logos/TrainingData/VW/own/"
    img_list = glob.glob(img_dir)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(10,10))

    for path, i in zip(img_list, range(len(img_list))):
        img = cv2.imread(path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cl1 = clahe.apply(gray)
        cv2.imwrite(out_dir+str(i)+".jpg", cl1)

def b2g():
    
    img_dir = "/home/kojak/raw_data/logos/TrainingData/VW/own/*.jpg"
    out_dir = "/home/kojak/raw_data/logos/TrainingData/VW/z/"
    img_list = glob.glob(img_dir)

    img_names = []
    for item in img_list:
        temp = item.split("/")
        img_names.append(temp[len(temp)-1])
    print (img_names)

    for path, img_name, i in zip(img_list, img_names, range(len(img_list))):
        print ("%d / %d" %(i, len(img_list)))
        gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        #th, imgt1 = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY);
        th = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
        cv2.imwrite(out_dir+img_name, th)
    


def create_samples_single_img():
        
        # This function reads in the images in the desired folder,creates copies of those images with
        # applied distortions to increase amount of samples for cascade training
        

        # Folder containing the images that needs to be used to create the samples
        sample_list = glob.glob("/home/kojak/raw_data/logos/TrainingData/Ford_own/*.jpg") # <-- make sure the file extention is correct, or just use '*' instead of '*.jpg' for
                                                                      # multiple file types. Then the selected directory must contain ONLY image files
        n_samples = len(sample_list)
        
        # Parameters: 
        num_samples = N_SAMPLES # Number of samples to create per image
        output_file_name = "clone" # Desired name of the output files. Files will be output as "output_file_name0.vec, output_file_name1.vec, etc"
        width = SAMPLE_WIDTH
        height = SAMPLE_HEIGHT

        for path, i in zip(sample_list, range(n_samples)):
            exec_str = "opencv_createsamples -img %s -bg bg.txt -info ford%d.info -pngoutput -maxxangle 0.5 -maxyangle 0.5 -maxzangle 0.5 -num 350" % (path, i)
            os.system(exec_str)

def create_info_file():
        
        # This function creates the .info file
        print("Creating .info file: ")
        f = open(info_dir, "w+")
        for path, count in zip(POS_SAMPLE_LIST, range(N_POS_SAMPLES)):
                print ("%d / %d" %(count, N_POS_SAMPLES))
                img = cv2.imread(path)
                height, width = img.shape[:2]
                output_str = ""+path+" 1 0 0 %d %d\n" %(int(width), int(height))
                f.write(output_str)

        f.close()
        print("Done.")
        

def create_bg_file():
        
        # This function creates the file containing information about the negative (background) images
        
        print("Creating background file: ")
        f = open(bg_dir, "w+")
        for path, count in zip(NEG_SAMPLE_LIST, range(N_NEG_SAMPLES)):
                print ("%d / %d" %(count, N_NEG_SAMPLES))
                f.write(path+"\n")
        f.close()
        print("Done.")
        

def create_vec(vec_dir=vec_dir, n_samples=N_POS_SAMPLES):

        # This function creates the .vec file

        print("Creating .vec file: ")
        exec_str = "opencv_createsamples -info %s -num %d -w %d -h %d -vec %s" % (INFO_FILENAME, n_samples, SAMPLE_WIDTH, SAMPLE_HEIGHT, vec_dir)
        print (exec_str) 
        os.system(exec_str)
        

def train_cascade(vec_dir=vec_dir):
        
        # This function initiates the training of the classifer
        # NOTE: This can take extremely long! (20 stages, 1000 pos and 4000 neg can take 3+ hours!)
        exec_str = ("opencv_traincascade -data %s -vec %s -bg %s -numPos 9700 -numNeg 4500 "
                    "-numStages %d -w %d -h %d -maxFalseAlarmRate %f -minHitRate %f "
                    "-featureType LBP -numThreads %d -precalcValBufSize 4096 -precalcIdxBufSize 4096" % (BASE_OUTPUT_DIR, vec_dir, bg_dir,NUM_STAGES, SAMPLE_WIDTH, SAMPLE_HEIGHT, MAX_FALSE_ALARM_RATE, MIN_HITRATE, THREADS))
        print (exec_str)  
        os.system(exec_str)
        
#resize_images("/home/kojak/raw_data/logos/TrainingData/VW/*.jpg",25,25)
#change_clahe()
#b2g()
#create_info_file()
#create_bg_file()       
#create_vec(n_samples=11000)
#train_cascade(vec_dir="/home/kojak/raw_data/logos/TrainingData/VW/clones/merged.vec")
train_cascade()
#create_samples_single_img()
#merge_vec_files("/home/kojak/raw_data/logos/TrainingData/VW/clones/", "/home/kojak/raw_data/logos/TrainingData/VW/clones/merged.vec")


