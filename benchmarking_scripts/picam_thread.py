# Adapted from pyimagesearch.com

from picamera import PiCamera
from picamera.array import PiRGBArray
from threading import Thread
import cv2

class PiCamStream:
    """
        Class that initialises the PiCamera object and opens the video stream.
        The latest frame from the stream is saved to an instance variable
        that can be accessed with a get() method.

        Frame capture and saving process is done in a thread
    """
    def __init__(self, res=(640, 480), fps=32):
        self.frame = None # The latest frame captured from the video stream
        self.active = False
        
        # Initialise camera
        self.camera = PiCamera()
        self.camera.resolution = res
        self.camera.framerate = fps
        self.raw = PiRGBArray(self.camera, size=res)
        self.stream = self.camera.capture_continuous(self.raw, format="bgr", use_video_port=True)


    def capture(self):
        # Continiously save the captured frames to the frame class variable
        # until stop() is called
        for frame in self.stream:

            self.frame=frame.array
            self.raw.truncate(0)

            if self.active == False:
                # Close all 'streams'
                self.stream.close()
                self.raw.close()
                self.camera.close()
                return
            
    def start(self):
        # Spawns capture() as a thread
        self.active = True
        Thread(target=self.capture, args=()).start()
        return self

    def stop(self):
        self.active = False

    def get_frame(self):
        return self.frame


    
