from FPS import FPS
from picam_thread import PiCamStream
import cv2, time

num_frames = 1000
picam_fps = 90

stream = PiCamStream(fps=picam_fps)
stream.start()
time.sleep(1.5)

total_fps = 0

def bench_thread(rnum_frames, picam_fps):

    print ("Threaded picamera sampling")
    
    fps = FPS().start()

    while fps.frames <= num_frames:
        frame = stream.get_frame()
        fps.count_frame()
        
    fps.stop()
    fps.display_result(test_name="Threaded picamera")
    global total_fps
    total_fps += fps.fps()
    

for i in range(100):
    bench_thread(num_frames, picam_fps)

print("Avg fps: %f" % (total_fps/100.0))
stream.stop()
