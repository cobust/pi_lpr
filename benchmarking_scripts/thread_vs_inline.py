from FPS import FPS
from threading import Thread
from picam_thread import PiCamStream
from picamera import PiCamera
from picamera.array import PiRGBArray
import cv2, time

res = (640, 480)
num_frames = 500
picam_fps = 60

def benchmark(res, num_frames, picam_fps):

    """
        Benchmarks the throughput of inline frame sampling and threaded
        frame sampling.

        ote that the PiCamera itself is initialised to capture at 60 fps
        in both cases
    """
    
    camera = PiCamera()
    camera.resolution = res
    camera.framerate = picam_fps
    raw = PiRGBArray(camera, size=res)
    stream = camera.capture_continuous(raw, format="bgr", use_video_port=True)

    print ("Inline picamera sampling")
    time.sleep(1)
    fps = FPS().start()

    i = 0
    for f in stream:
        
        if i >= num_frames:
            break
        
        frame = f.array
        raw.truncate(0)
        fps.count_frame()
        i += 1
       

    fps.stop()
    fps.display_result(test_name="Inline picamera")
    inline_fps = fps.fps()

    stream.close()
    raw.close()
    camera.close()

    print ("\nThreaded picamera sampling")
    stream = PiCamStream().start()
    time.sleep(1)
    fps.reset()
    fps.start()

    while fps.frames < num_frames:
        frame = stream.get_frame()
        #cv2.imshow("Stream", frame)
        #key = cv2.waitKey(1) & 0xFF
        fps.count_frame()
        
    fps.stop()
    fps.display_result(test_name="Threaded picamera")
    threaded_fps = fps.fps()
    stream.stop()
    #cv2.destroyAllWindows()

    perf_increase = (threaded_fps/inline_fps)*100
    print("Threaded implementation gives and increase of " + str(perf_increase) + "%")

def show_streams(res, picam_fps):

    """
        Shows the output of inline sampling and threaded sampling to
        get a feel for the 'smoothness' difference between the two
        implementations.

        Note that the PiCamera itself is initialised to capture at 60 fps
        in both cases
    """

    camera = PiCamera()
    camera.resolution = res
    camera.framerate = picam_fps
    raw = PiRGBArray(camera, size=res)
    inline_stream = camera.capture_continuous(raw, format="bgr", use_video_port=True)

    time.sleep(1)

    for f in inline_stream:
        
        frame = f.array
        cv2.namedWindow("Inline_stream", cv2.WINDOW_AUTOSIZE)  
        cv2.imshow("Inline_stream", frame)
        raw.truncate(0)
        c = cv2.waitKey(1) % 0x100
        
        if c == 27 or c == 10:
            inline_stream.close()
            raw.close()
            camera.close()
            break
        
    cv2.destroyAllWindows()
    thread_stream = PiCamStream().start()
    time.sleep(1)
    while True:
        
        frame = thread_stream.get_frame()
        cv2.namedWindow("Thread_stream", cv2.WINDOW_AUTOSIZE)  
        cv2.imshow("Thread_stream", frame)
        c = cv2.waitKey(1) % 0x100
    
        if c == 27 or c == 10:
            thread_stream.stop()
            break
        
    cv2.destroyAllWindows()
                
        
benchmark(res, num_frames, picam_fps)
#show_streams(res, picam_fps)
