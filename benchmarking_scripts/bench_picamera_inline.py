from FPS import FPS
from picamera import PiCamera
from picamera.array import PiRGBArray
import cv2, time

res = (640, 480)
num_frames = 1000
picam_fps = 90

def bench_picam(res, num_frames, picam_fps):
    
    camera = PiCamera()
    camera.resolution = res
    camera.framerate = picam_fps
    raw = PiRGBArray(camera, size=res)
    stream = camera.capture_continuous(raw, format="bgr", use_video_port=True)

    print ("Inline picamera sampling")
    time.sleep(1.5)

    i = 0
    fps = FPS().start()
    for f in stream:
        
        if i >= num_frames:
            break
        
        frame = f.array
        raw.truncate(0)
        fps.count_frame()
        i += 1
       
    fps.stop()
    stream.close()
    raw.close()
    camera.close()

    fps.display_result(test_name="Inline picamera")

for i in range(5):
    bench_picam(res, num_frames, picam_fps)
